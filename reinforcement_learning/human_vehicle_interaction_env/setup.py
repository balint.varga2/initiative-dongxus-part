from setuptools import setup

setup(
    name="human_vehicle_interaction_env",
    version="0.0.1",
    install_requires=["gym", "numpy"]
)