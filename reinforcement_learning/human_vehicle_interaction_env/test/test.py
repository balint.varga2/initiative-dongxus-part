import os
import gym
from stable_baselines3 import PPO
from stable_baselines3 import DQN
from utils.controller.recurrent_ppo_control import RecurrentPPOAgent
from utils.controller.non_linear_mpc_explicit_1d import NonLinearMPC_explicit
from utils.controller.rule_based_controller import RuleBasedController
from utils.controller.non_linear_mpc_implicit_1d import NonLinearMPC_implicit
import human_vehicle_interaction_env
from human_vehicle_interaction_env.wrappers import *

controller = RecurrentPPOAgent()
mode = 'DQN'


def test_env(iteration=5):
    env = gym.make('hvi-2d', cross=True, human_model='SFM', shuffle=False)
    if mode == 'DQN':
        env = DiscreteAction(env)

    if mode == 'PPO':
        model_weights_path = os.getenv(
            "HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/PPO/sb3/best_model'
        agent = PPO.load(model_weights_path)
    elif mode == 'DQN':
        model_weights_path = os.getenv(
            "HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/DQN/sb3/best_model'
        agent = DQN.load(model_weights_path, env=env, custom_objects={'observation_space': env.observation_space, 'action_space': env.action_space})
    else:
        agent = controller

    for i in range(iteration):
        obs = env.reset()
        count = 0
        while True:
            if mode == 'PPO' or mode == 'DQN':
                act, _ = agent.predict(obs, deterministic=True)
            else:
                ped = env.get_info()['pedestrian']
                veh = env.get_info()['vehicle']
                act = agent.eval(ped, veh)[0]

            # print(f'current acc: {act}')
            obs, reward, done, info = env.step(act)
            env.render(mode='human')
            count += 1

            if done or count >= 250:
                break


if __name__ == '__main__':
    test_env()

