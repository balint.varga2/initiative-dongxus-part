import gym
from gym import spaces
import numpy as np


class WithoutIntention(gym.ObservationWrapper):
    def __init__(self, env):
        super().__init__(env)
        self.observation_space = spaces.Box(-np.inf, np.inf, shape=(5,), dtype=np.float32)

    def observation(self, observation):
        return observation[:-1]


if __name__ == '__main__':
    import human_vehicle_interaction_env
    env = gym.make('hvi-v2', cross=True, human_model='MDP', shuffle=True)
    env = WithoutIntention(env)
    obs = env.reset()
    obs, _, _, _ = env.step(1.0)
    print(f'current obs: {obs}')
    print(f'current obs: {env.get_obs()}')
