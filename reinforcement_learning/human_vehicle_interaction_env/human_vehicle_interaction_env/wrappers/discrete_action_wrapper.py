import gym
from gym import spaces
import numpy as np


class DiscreteAction(gym.ActionWrapper):
    def __init__(self, env):
        super().__init__(env)

        self.discrete_acc = np.array([-4.0, -2.0, 0.0, 2.0, 4.0])
        self.action_space = spaces.Discrete(len(self.discrete_acc))

    def action(self, action):
        return self.discrete_acc[action]


if __name__ == '__main__':
    import human_vehicle_interaction_env
    env = gym.make('hvi-2d', cross=True, human_model='MDP', shuffle=True)
    env = DiscreteAction(env)
    obs = env.reset()
    print(f'action: {env.action_space}')

