from .HumanVehicleInteractionEnv1D import *
from utils.human_motion.pedestrian_SFM_2d import social_force_model_2d
from utils.human_motion.pedestrian_MDP_2d import predict_ped_with_MDP_2d


class HumanVehicleInteractionEnv2D(HumanVehicleInteractionEnv1D):
    def __init__(self, cross=True, human_model='SFM', shuffle=False):
        super().__init__(cross=cross, human_model=human_model, shuffle=shuffle)

    def move_pedestrian(self):
        if self.human_model == 'constant':
            if self.is_cross is True:
                pass
            else:
                self.pedestrian.yspeed_ms_var = 0.1
        elif self.human_model == 'MDP':
            pedestrian_speed_next = predict_ped_with_MDP_2d(self.pedestrian, self.vehicle, self.is_cross)
            self.pedestrian.xspeed_ms_var = pedestrian_speed_next[0]
            self.pedestrian.yspeed_ms_var = pedestrian_speed_next[1]
        elif self.human_model == 'SFM':
            pedestrian_speed_next = social_force_model_2d(self.pedestrian, self.vehicle, self.is_cross)
            self.pedestrian.xspeed_ms_var = pedestrian_speed_next[0]
            self.pedestrian.yspeed_ms_var = pedestrian_speed_next[1]

        self.pedestrian.x_var += self.pedestrian.xspeed_ms_var * self.time_step
        self.pedestrian.y_var += self.pedestrian.yspeed_ms_var * self.time_step
