import gym
import yaml
import os
import torch.nn as nn
from sb3_contrib import RecurrentPPO
from stable_baselines3.common.vec_env import SubprocVecEnv, DummyVecEnv, VecMonitor
import wandb
import human_vehicle_interaction_env

wandb.login(key='fdb2c065668b19790af973cb1f1c0177fae58add')
run = wandb.init(project='HumanVehicleInteraction',
                 sync_tensorboard=True)


# Create environment
# env = human_vehicle_interaction_env()
# envs = DummyVecEnv([lambda: human_vehicle_interaction_env()] * 2)
envs = DummyVecEnv([lambda: gym.make('hvi-v0')] * 2)
envs = VecMonitor(envs)
log_dir = 'logs'
os.makedirs(log_dir, exist_ok=True)

model_params = dict()
with open('model_hyperparams.yaml', 'r') as file:
    model_hyperparams = yaml.safe_load(file)

for key in model_hyperparams:
    model_params[key] = model_hyperparams[key]

activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU}
model_params['policy_kwargs']['activation_fn'] = activation_fn[model_params['policy_kwargs']['activation_fn']]


model = RecurrentPPO("MlpLstmPolicy", envs, verbose=1, **model_params, tensorboard_log=log_dir)
# Train the agent and display a progress bar
model.learn(total_timesteps=int(4e5), progress_bar=True)
# Save the agent
model.save("basic_policy")

wandb.finish()
