import gym
from human_vehicle_interaction_env.wrappers import WithoutIntention


def make_env(env_id: str, rank: int, seed: int = 0, cross: bool = True, human_model: str = "SFM", shuffle: bool = False, with_intention: bool = True):
    """
    Utility function for multiprocessed env.

    :param env_id: the environment ID
    :param num_env: the number of environments you wish to have in subprocesses
    :param seed: the inital seed for RNG
    :param rank: index of the subprocess
    """
    def _init():
        env = gym.make(env_id, cross=cross, human_model=human_model, shuffle=shuffle)
        if not with_intention:
            env = WithoutIntention(env)
        env.reset(seed=seed + rank)
        return env
    return _init
