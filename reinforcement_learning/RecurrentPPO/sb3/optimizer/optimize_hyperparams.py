import gym
import optuna
import yaml
import torch.nn as nn
from stable_baselines3 import PPO
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.evaluation import evaluate_policy
from human_vehicle_interaction_env import HumanVehicleInteractionEnv


def optimize_params(trial):
    """
    Sampler for PPO hyperparams.
    :param trial:
    :return:
    """
    batch_size = trial.suggest_categorical("batch_size", [5, 10])
    n_steps = trial.suggest_categorical("n_steps", [10, 20, 30])
    gamma = trial.suggest_categorical("gamma", [0.9, 0.95, 0.98, 0.99, 0.995, 0.999])
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1e-2, log=True)
    lr_schedule = "constant"
    # Uncomment to enable learning rate schedule
    # lr_schedule = trial.suggest_categorical('lr_schedule', ['linear', 'constant'])
    clip_range = trial.suggest_categorical("clip_range", [0.1, 0.2, 0.3])
    n_epochs = trial.suggest_categorical("n_epochs", [5, 10, 15])
    gae_lambda = trial.suggest_categorical("gae_lambda", [0.8, 0.9, 0.92, 0.95, 0.98, 0.99])
    max_grad_norm = trial.suggest_categorical("max_grad_norm", [0.3, 0.5, 0.6, 0.7, 0.8, 0.9])
    vf_coef = trial.suggest_float("vf_coef", 0, 1)
    net_arch = trial.suggest_categorical("net_arch", ["small", "medium"])
    # Uncomment for gSDE (continuous actions)
    # log_std_init = trial.suggest_uniform("log_std_init", -4, 1)
    # Uncomment for gSDE (continuous action)
    # sde_sample_freq = trial.suggest_categorical("sde_sample_freq", [-1, 8, 16, 32, 64, 128, 256])
    activation_fn = trial.suggest_categorical("activation_fn", ["tanh", "relu"])

    if batch_size >= n_steps:
        batch_size = n_steps // 2

    # if lr_schedule == "linear":
    #     learning_rate = linear_schedule(learning_rate)

    # Independent networks usually work best
    # when not working with images
    net_arch = {
        "small": dict(pi=[64, 64], vf=[64, 64]),
        "medium": dict(pi=[256, 256], vf=[256, 256]),
    }[net_arch]

    activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU}[activation_fn]

    return {
        "n_steps": n_steps,
        "batch_size": batch_size,
        "gamma": gamma,
        "learning_rate": learning_rate,
        "clip_range": clip_range,
        "n_epochs": n_epochs,
        "gae_lambda": gae_lambda,
        "max_grad_norm": max_grad_norm,
        "vf_coef": vf_coef,
        # "sde_sample_freq": sde_sample_freq,
        "policy_kwargs": dict(
            # log_std_init=log_std_init,
            net_arch=net_arch,
            activation_fn=activation_fn,
        ),
    }


def objective(trial):

    try:
        env = HumanVehicleInteractionEnv()
        env = gym.wrappers.TimeLimit(env, max_episode_steps=250)
        env = Monitor(env)
        env = DummyVecEnv([lambda: env])

        model_params = optimize_params(trial)

        model = PPO("MlpPolicy", env, **model_params)
        model.learn(total_timesteps=int(3e4), progress_bar=True)

        mean_reward, _ = evaluate_policy(model, env, n_eval_episodes=5)

        env.close()

        return mean_reward
    except Exception as e:
        return -1000


study = optuna.create_study(direction='maximize')
study.optimize(objective, n_trials=100)

net_arch = {
        "small": dict(pi=[64, 64], vf=[64, 64]),
        "medium": dict(pi=[256, 256], vf=[256, 256]),
    }

best_params = study.best_params

if best_params['batch_size'] >= best_params['n_steps']:
    best_params['batch_size'] = best_params['n_steps'] // 2

best_params_yaml = f"""
n_steps: {best_params['n_steps']}
batch_size: {best_params['batch_size']}
gamma: {best_params['gamma']}
learning_rate: {best_params['learning_rate']}
clip_range: {best_params['clip_range']}
n_epochs: {best_params['n_epochs']}
gae_lambda: {best_params['gae_lambda']}
max_grad_norm: {best_params['max_grad_norm']}
vf_coef: {best_params['vf_coef']}
policy_kwargs:
    net_arch: {net_arch[best_params['net_arch']]}
    activation_fn: {best_params['activation_fn']}
"""

best_params_yaml = yaml.safe_load(best_params_yaml)

with open('model_hyperparams.yaml', 'w') as file:
    yaml.dump(best_params_yaml, file)
