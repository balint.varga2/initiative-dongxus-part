import gym
import yaml
import os
import torch.nn as nn
from sb3_contrib import RecurrentPPO
from stable_baselines3.common.vec_env import SubprocVecEnv, DummyVecEnv, VecMonitor
import wandb
from make_env import make_env
import human_vehicle_interaction_env

LOAD = False
env_id = 'hvi-2d'
cross = True
human_model = 'MDP'
shuffle = False
with_intention = True

wandb.login(key='fdb2c065668b19790af973cb1f1c0177fae58add')
run = wandb.init(project='HumanVehicleInteraction',
                 name='recurrent_PPO',
                 sync_tensorboard=True)


def main():
    # Create environment
    log_dir = 'logs'
    os.makedirs(log_dir, exist_ok=True)

    model_params = dict()
    with open('model_hyperparams.yaml', 'r') as file:
        model_hyperparams = yaml.safe_load(file)

    for key in model_hyperparams:
        model_params[key] = model_hyperparams[key]

    activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU}
    model_params['policy_kwargs']['activation_fn'] = activation_fn[model_params['policy_kwargs']['activation_fn']]

    if not LOAD:
        envs = DummyVecEnv([make_env(env_id=env_id, rank=i, cross=cross, human_model=human_model, with_intention=with_intention, shuffle=shuffle) for i in range(4)])
        envs = VecMonitor(envs)
        model = RecurrentPPO("MlpLstmPolicy", envs, verbose=1, **model_params, tensorboard_log=log_dir)
        # Train the agent and display a progress bar
        model.learn(total_timesteps=int(2e6), progress_bar=True)
    else:
        envs = DummyVecEnv([make_env(env_id=env_id, rank=i, cross=cross, human_model=human_model, with_intention=with_intention, shuffle=shuffle) for i in range(4)])
        envs = VecMonitor(envs)
        model = RecurrentPPO.load('recurrent_ppo_policy', env=envs, verbose=1, tensorboard_log=log_dir, reset_num_timesteps=False)
        model.learn(total_timesteps=int(4e5), progress_bar=True)

    run.finish()
    # Save the agent
    model.save("recurrent_ppo_policy")


if __name__ == '__main__':
    main()

