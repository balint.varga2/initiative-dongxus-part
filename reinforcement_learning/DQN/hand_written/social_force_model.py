import numpy as np
import scipy.special

ROAD_WIDTH = 4.0


def social_force_model(pedestrian, vehicle):

    speed_ped_optimal = - pedestrian.yspeed_ref_para
    speed_ped_fast = - pedestrian.yspeed_max_para
    ped_target_y = - 0.5 * ROAD_WIDTH

    aggressivity = 0.0
    relaxation_time = 0.02
    dt = 0.02

    collision_area_x = 2.2
    collision_area_y = 1.5
    reaction_area_y = 6.5

    # Modify the variables for the coordinate system of this function:
    if vehicle.y_var < pedestrian.y_init_var:
        position_ped = pedestrian.y_var - vehicle.y_var
        speed_ped = pedestrian.yspeed_ms_var
    else:
        position_ped = vehicle.y_var - pedestrian.y_var
        speed_ped = -pedestrian.yspeed_ms_var

    if vehicle.x_init_var < pedestrian.x_var:
        position_veh = pedestrian.x_var - vehicle.x_var
        speed_veh = vehicle.xspeed_ms_var
    else:
        position_veh = vehicle.x_var - pedestrian.x_var
        speed_veh = - vehicle.xspeed_ms_var


    if speed_veh <= 0.1:
        return - pedestrian.yspeed_ref_para if vehicle.y_var < pedestrian.y_init_var else pedestrian.yspeed_ref_para

    def time_predicted_to_position_ped(tau, position_actual, v_actual, v_optimal, position_ped_target, aggressivity):
        res = aggressivity + \
              (-position_actual + position_ped_target - tau * (v_actual - v_optimal) + tau * v_optimal * scipy.special.lambertw( \
                  np.exp((-position_ped_target + position_actual + tau * (v_actual - v_optimal)) / (tau * v_optimal)) * (v_actual - v_optimal) / v_optimal) \
               ) / v_optimal
        return res

    def speed_necessary_predicted_for_position_ped_at_time(tau, position_actual, v_actual, time, position_ped_target,
                                                           aggressivity):
        # equation from matlab script considering aggressivity
        t = time - aggressivity
        res = (position_actual - position_ped_target + tau * v_actual - tau * v_actual * np.exp(min(200, -t / tau))) / \
              (- t + tau - tau * np.exp(min(200, -t / tau)) + 0.01)
        return res

    is_veh_go_through = position_veh < - collision_area_x

    is_ped_in_reaction_area = 0 <= position_ped <= reaction_area_y
    is_ped_in_collision_area = position_ped <= collision_area_y

    is_veh_in_collision_area = -collision_area_x <= position_veh <= collision_area_x

    if not is_veh_go_through and is_ped_in_reaction_area:
        # pedestrian in reaction area and collision possible
        if is_veh_in_collision_area:
            # vehicle in collision area
            if not is_ped_in_collision_area:
                # pedestrian not in collision area
                t_veh_exit = (position_veh + collision_area_x) / speed_veh
                speed_ped_necessary_cross_second = - (position_ped - collision_area_y) / t_veh_exit
                speed_ped_optimal = speed_ped_necessary_cross_second if abs(speed_ped_necessary_cross_second) <= abs(
                    speed_ped_fast) else speed_ped_fast
            else:
                return 0.0
        else:
            if not is_ped_in_collision_area:
                # pedestrian not in collision area
                t_veh_entry = (position_veh - collision_area_x) / speed_veh
                t_veh_exit = (position_veh + collision_area_x) / speed_veh
                t_ped_entry = time_predicted_to_position_ped(relaxation_time, position_ped, speed_ped,
                                                             speed_ped_optimal, collision_area_y, 0)
                t_ped_exit = time_predicted_to_position_ped(relaxation_time, position_ped, speed_ped,
                                                            speed_ped_optimal, ped_target_y, aggressivity)
                if not ((t_ped_exit <= t_veh_entry) or (t_veh_exit <= t_ped_entry)):
                    # collision expected
                    speed_ped_necessary_cross_first = speed_necessary_predicted_for_position_ped_at_time(
                        relaxation_time, position_ped, speed_ped, t_veh_entry, ped_target_y, aggressivity)
                    speed_ped_necessary_cross_second = speed_necessary_predicted_for_position_ped_at_time(
                        relaxation_time, position_ped, speed_ped, t_veh_exit, collision_area_y, 0)

                    speed_ped_optimal = speed_ped_necessary_cross_first if abs(speed_ped_necessary_cross_first) <= abs(
                        speed_ped_fast) else speed_ped_necessary_cross_second
            else:
                # pedestrian in collision area
                t_veh_entry = (position_veh - collision_area_x) / speed_veh
                t_ped_exit = time_predicted_to_position_ped(relaxation_time, position_ped, speed_ped,
                                                            speed_ped_optimal, ped_target_y, aggressivity)
                if not (t_ped_exit <= t_veh_entry):
                    # collision expected
                    speed_ped_necessary_cross_first = speed_necessary_predicted_for_position_ped_at_time(
                        relaxation_time, position_ped, speed_ped, t_veh_entry, ped_target_y, aggressivity)

                    speed_ped_optimal = speed_ped_necessary_cross_first if abs(speed_ped_necessary_cross_first) <= abs(
                        speed_ped_fast) else speed_ped_fast

    acceleration_ped = 1 / relaxation_time * (speed_ped_optimal - speed_ped)
    speed_ped += acceleration_ped * dt
    if vehicle.y_var > pedestrian.y_init_var: speed_ped = - speed_ped

    return speed_ped

