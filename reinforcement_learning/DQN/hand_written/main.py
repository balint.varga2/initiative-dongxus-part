import math
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from collections import namedtuple, deque
from itertools import count

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from dqn_utils import DQN
from sim_model_objects import Object
from social_force_model import social_force_model
TIME_STEP = 0.1
action_space = [-2.0, 0.0, 2.0]

# set up matplotlib
is_ipython = 'inline' in matplotlib.get_backend()
# if is_ipython:
#     from IPython import display

plt.ion()

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))


# -------------------------------------------------------------------

class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([],maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

# ----------------------------------------------------------------------------------------


def step(action, pedestrian, vehicle):

    acc = action_space[action]
    ped_veh_distance = np.sqrt((pedestrian.x_var - vehicle.x_var) ** 2 + (pedestrian.y_var - vehicle.y_var) ** 2)
    is_collision = ped_veh_distance < 2.0 and vehicle.x_var < pedestrian.x_var and vehicle.y_var < pedestrian.y_var and vehicle.xspeed_ms_var > 0.5
    is_veh_at_end = vehicle.x_var > 89.0
    is_ped_at_end = pedestrian.y_var < 1.0
    is_veh_crossed = pedestrian.x_var < vehicle.x_var
    is_ped_crossed = pedestrian.y_var < vehicle.y_var - 1.5

    reward = 0.0
    # reward
    if not is_veh_crossed and not is_ped_crossed:
        if is_collision:
            reward += -1000

        time_to_collision = abs(abs((pedestrian.x_var - vehicle.x_var)/(vehicle.xspeed_ms_var + 0.001)) - \
                                abs(abs((pedestrian.y_var - vehicle.y_var)/(pedestrian.yspeed_ms_var + 0.001))))
        reward += time_to_collision - 3.0
    k = 1 / vehicle.xspeed_ref_para
    if vehicle.xspeed_ms_var <= 0:
        reward += -1
    elif vehicle.xspeed_ms_var > vehicle.xspeed_ref_para:
        reward += -10
    else:
        reward += (1 - k * (vehicle.xspeed_ref_para - vehicle.xspeed_ms_var)) * 2

    # observation
    pedestrian.yspeed_ms_var = social_force_model(pedestrian, vehicle)
    pedestrian.y_var += pedestrian.yspeed_ms_var * TIME_STEP
    vehicle.x_var += vehicle.xspeed_ms_var * TIME_STEP + 0.5 * acc * TIME_STEP ** 2
    vehicle.xspeed_ms_var += acc * TIME_STEP

    observation = [pedestrian.x_var - vehicle.x_var, pedestrian.y_var - vehicle.y_var, pedestrian.yspeed_ms_var, vehicle.xspeed_ms_var]

    # terminated
    if is_veh_at_end or is_ped_at_end:
        terminated = True
    else:
        terminated = False

    return observation, reward, terminated

# -----------------------------------------------------------------------------

# BATCH_SIZE is the number of transitions sampled from the replay buffer
# GAMMA is the discount factor as mentioned in the previous section
# EPS_START is the starting value of epsilon
# EPS_END is the final value of epsilon
# EPS_DECAY controls the rate of exponential decay of epsilon, higher means a slower decay
# TAU is the update rate of the target network
# LR is the learning rate of the AdamW optimizer
BATCH_SIZE = 128
GAMMA = 0.99
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 1000
TAU = 0.005
LR = 1e-4

# Get number of actions
n_actions = len(action_space)
n_observations = 4

policy_net = DQN(n_observations, n_actions).to(device)
target_net = DQN(n_observations, n_actions).to(device)
target_net.load_state_dict(policy_net.state_dict())

optimizer = optim.AdamW(policy_net.parameters(), lr=LR, amsgrad=True)
memory = ReplayMemory(10000)

steps_done = 0


def select_action(state):
    global steps_done
    sample = random.random()
    eps_threshold = EPS_END + (EPS_START - EPS_END) * \
        math.exp(-1. * steps_done / EPS_DECAY)
    steps_done += 1
    if sample > eps_threshold:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            return policy_net(state).max(1)[1].view(1, 1)
    else:
        return torch.tensor([random.sample([0, 1, 2], 1)], device=device, dtype=torch.long)

# ---------------------------------------------------------------------------

episode_durations = []


def plot_durations(show_result=False):
    plt.figure(1)
    durations_t = torch.tensor(episode_durations, dtype=torch.float)
    if show_result:
        plt.title('Result')
    else:
        plt.clf()
        plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('Duration[s]')
    plt.plot(durations_t.numpy())
    # Take 100 episode averages and plot them too
    # if len(durations_t) >= 100:
    #     means = durations_t.unfold(0, 100, 1).mean(1).view(-1)
    #     means = torch.cat((torch.zeros(99), means))
    #     plt.plot(means.numpy())

    plt.pause(0.001)  # pause a bit so that plots are updated
    # if is_ipython:
    #     if not show_result:
    #         display.display(plt.gcf())
    #         display.clear_output(wait=True)
    #     else:
    #         display.display(plt.gcf())

# ---------------------------------------------------------------------------------

def optimize_model():
    if len(memory) < BATCH_SIZE:
        return
    transitions = memory.sample(BATCH_SIZE)
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                          batch.next_state)), device=device, dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_state
                                                if s is not None])
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken. These are the actions which would've been taken
    # for each batch state according to policy_net
    state_action_values = policy_net(state_batch).gather(1, action_batch)

    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(BATCH_SIZE, device=device)
    with torch.no_grad():
        next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0]
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch

    # Compute Huber loss
    criterion = nn.SmoothL1Loss()
    loss = criterion(state_action_values, expected_state_action_values.unsqueeze(1))

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    # In-place gradient clipping
    torch.nn.utils.clip_grad_value_(policy_net.parameters(), 100)
    optimizer.step()

# ----------------------------------------------------------------------------------------
ped_init_y = [14.0, 13.5, 13.0, 8.0]
veh_init_x = [45.0, 47.0, 50.0, 60.0]

if torch.cuda.is_available():
    num_episodes = 6000
else:
    num_episodes = 250

for i in range(1):

    for i_episode in range(num_episodes):

        ped = Object()
        ped.yspeed_ms_var = -1.4
        ped.x_var = 60.0
        ped.y_var = ped_init_y[i]
        ped.yspeed_ref_para = 1.4
        ped.yspeed_max_para = 2.5
        ped.intention_var = 1.0  # between 0 and 1
        ped.y_init_var = ped_init_y[i]

        veh = Object()
        veh.xspeed_ms_var = 7.0
        veh.x_var = veh_init_x[i]
        veh.y_var = 10.0
        veh.xspeed_ref_para = 7.0
        veh.xspeed_max_para = 10.0
        veh.x_init_var = veh_init_x[i]

        state = [ped.x_var - veh.x_var, ped.y_var - veh.y_var, ped.yspeed_ms_var, veh.xspeed_ms_var]
        state = torch.tensor(state, dtype=torch.float32, device=device).unsqueeze(0)

        for t in count():
            action = select_action(state)
            observation, reward, terminated = step(action.item(), ped, veh)
            reward = torch.tensor([reward], device=device)

            if terminated:
                next_state = None
            else:
                next_state = torch.tensor(observation, dtype=torch.float32, device=device).unsqueeze(0)

            # Store the transition in memory
            memory.push(state, action, next_state, reward)

            # Move to the next state
            state = next_state

            # Perform one step of the optimization (on the policy network)
            optimize_model()

            # Soft update of the target network's weights
            # θ′ ← τ θ + (1 −τ )θ′
            target_net_state_dict = target_net.state_dict()
            policy_net_state_dict = policy_net.state_dict()
            for key in policy_net_state_dict:
                target_net_state_dict[key] = policy_net_state_dict[key]*TAU + target_net_state_dict[key]*(1-TAU)
            target_net.load_state_dict(target_net_state_dict)

            if terminated:
                episode_durations.append((t + 1) * 0.1)
                plot_durations()
                break

print('Complete')
plot_durations(show_result=True)
plt.ioff()
plt.show()

torch.save(policy_net.state_dict(), "policy_net_weights.pth")











