class Object():
    def __init__(self):
        self.x_var = 0.0
        self.y_var = 0.0

        self.xspeed_ms_var = 0.0
        self.yspeed_ms_var = 0.0

        self.xspeed_ref_para = 0.0
        self.yspeed_ref_para = 0.0

        self.xspeed_max_para = 0.0
        self.yspeed_max_para = 0.0

        self.intention_var = 0.0
        self.x_init_var = 0.0
        self.y_init_var = 0.0