import gym
import yaml
import os
import torch.nn as nn
from stable_baselines3 import DQN
from stable_baselines3.common.vec_env import SubprocVecEnv, DummyVecEnv, VecMonitor
import wandb
from make_env import make_env
import human_vehicle_interaction_env

LOAD = False    # if load trained model
env_id = 'hvi-2d'   # human-vehicle-interaction-2d means pedestrian moves in 2 directions
cross = True    # if pedestrian intents to cross in simulation, if cross, select MDP or SFM
human_model = 'constant'    # SFM, MDP, constant
shuffle = True  # if change pedestrian motion model when a new episode starts in simulation
with_intention = True   # if the pedestrian's intention is observable in simulation

# if don't use wandb, please comment this part
# wandb.login(key='')
# run = wandb.init(project='HumanVehicleInteraction',
#                 name='dqn_v1',
#                 sync_tensorboard=True)


def main():
    # Create environment
    log_dir = 'logs'
    os.makedirs(log_dir, exist_ok=True)

    # model_params = dict()
    # with open('model_hyperparams.yaml', 'r') as file:
    #     model_hyperparams = yaml.safe_load(file)
    #
    # for key in model_hyperparams:
    #     model_params[key] = model_hyperparams[key]
    #
    # activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU}
    # model_params['policy_kwargs']['activation_fn'] = activation_fn[model_params['policy_kwargs']['activation_fn']]

    if not LOAD:
        envs = DummyVecEnv([make_env(env_id=env_id, rank=i, cross=cross, human_model=human_model, with_intention=with_intention, shuffle=shuffle) for i in range(8)])
        envs = VecMonitor(envs)
        model = DQN("MlpPolicy", envs, verbose=1, tensorboard_log=log_dir)
        # Train the agent and display a progress bar
        model.learn(total_timesteps=int(5e6), progress_bar=True)
    else:
        envs = DummyVecEnv([make_env(env_id=env_id, rank=i, cross=cross, human_model=human_model, with_intention=with_intention, shuffle=shuffle) for i in range(8)])
        envs = VecMonitor(envs)
        model = DQN.load('ppo_policy', env=envs, verbose=1, tensorboard_log=log_dir, reset_num_timesteps=False)
        model.learn(total_timesteps=int(4e5), progress_bar=True)

    # run.finish()
    # Save the agent
    model.save("dqn_v1")


if __name__ == '__main__':
    main()
