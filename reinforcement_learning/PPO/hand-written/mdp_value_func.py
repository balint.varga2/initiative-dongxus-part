import numpy as np
import copy
ROAD_WIDTH = 2.0


def mdp_value_func(pedestrian, vehicle, horizon, time_step, q_s_a_table):
    actions = [-1.0, 0.0, 1.0]
    discount = 0.9

    ped_target_y = vehicle.y_var + 0.5 * ROAD_WIDTH * np.sign(vehicle.y_var - pedestrian.y_init_var)
    # define reward
    weights = np.array([100.0, 0.01 * np.exp(pedestrian.intention_var), 0.05])
    # term 1
    ped_veh_close_distance = 1.5
    ped_veh_distance = np.sqrt((vehicle.x_var - pedestrian.x_var) ** 2 + (vehicle.y_var - pedestrian.y_var) ** 2)
    ped_veh_distance_penalty = - float('inf') if ped_veh_distance <= ped_veh_close_distance else - 30.0 / ped_veh_distance ** 8
    # term 2
    ped_target_distance = - np.sqrt((ped_target_y - pedestrian.y_var) ** 2)
    # term 3
    if pedestrian.y_init_var > vehicle.y_var:
        ped_yspeed_penalty = - (pedestrian.yspeed_ms_var + pedestrian.yspeed_ref_para) ** 2
    else:
        ped_yspeed_penalty = - (pedestrian.yspeed_ms_var - pedestrian.yspeed_ref_para) ** 2

    reward_items = np.array([ped_veh_distance_penalty, ped_target_distance, ped_yspeed_penalty])

    is_pedestrian_crossed = pedestrian.y_var < ped_target_y if pedestrian.y_init_var > vehicle.y_var else pedestrian.y_var > ped_target_y
    if not is_pedestrian_crossed:
        reward = np.dot(weights, reward_items)
    else:
        reward = np.dot(weights[-1], reward_items[-1])
    if abs(pedestrian.yspeed_ms_var) > pedestrian.yspeed_max_para:
        reward = - float('inf')

    if horizon == 0:
        if not q_s_a_table.get('0'):  q_s_a_table['0'] = {}
        if not q_s_a_table['0'].get((pedestrian.y_var, pedestrian.yspeed_ms_var)):
            q_s_a_table['0'][(pedestrian.y_var, pedestrian.yspeed_ms_var)] = {}
        q_s_a_table['0'][(pedestrian.y_var, pedestrian.yspeed_ms_var)]['0.0'] = reward
        return reward

    opt_value = - float('inf')
    for action in actions:
        new_pedestrian = copy.deepcopy(pedestrian)
        new_vehicle = copy.deepcopy(vehicle)

        new_pedestrian.yspeed_ms_var += time_step * action
        new_pedestrian.y_var += time_step * new_pedestrian.yspeed_ms_var
        new_vehicle.x_var += time_step * vehicle.xspeed_ms_var
        new_value = mdp_value_func(new_pedestrian, new_vehicle, horizon - 1, time_step, q_s_a_table)
        q_s_a = reward + discount * new_value

        if not q_s_a_table.get(str(horizon)): q_s_a_table[str(horizon)] = {}
        if not q_s_a_table[str(horizon)].get((pedestrian.y_var, pedestrian.yspeed_ms_var)):
            q_s_a_table[str(horizon)][(pedestrian.y_var, pedestrian.yspeed_ms_var)] = {}
        q_s_a_table[str(horizon)][(pedestrian.y_var, pedestrian.yspeed_ms_var)][str(action)] = q_s_a

        if q_s_a > opt_value:
            opt_value = q_s_a

    return opt_value