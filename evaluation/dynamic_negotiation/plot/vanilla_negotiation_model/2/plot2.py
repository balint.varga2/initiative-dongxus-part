import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

f1 = open('gui_feedback_SFM.yaml','r')
f2 = open('gui_feedback_MDP.yaml','r')

model_state_SFM = list(yaml.safe_load_all(f1))[5::5]
model_state_MDP = list(yaml.safe_load_all(f2))[5::5]

veh_xspeed_SFM = []
ped_yspeed_SFM = []
veh_ped_distance_x_SFM = []
veh_ped_distance_y_SFM = []
time_SFM = []

veh_xspeed_MDP = []
ped_yspeed_MDP = []
veh_ped_distance_x_MDP = []
veh_ped_distance_y_MDP = []
time_MDP = []

for i, element in enumerate(model_state_SFM):
    if element is not None:
       veh_xspeed_SFM.append(element['vehicle_x_speed'])
       ped_yspeed_SFM.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_SFM.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_SFM.append(element['vehicle_y'] - element['pedestrian_y'])
       time_SFM.append(i * 0.1)

for i, element in enumerate(model_state_MDP):
    if element is not None:
       veh_xspeed_MDP.append(element['vehicle_x_speed'])
       ped_yspeed_MDP.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_MDP.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_MDP.append(element['vehicle_y'] - element['pedestrian_y'])
       time_MDP.append(i * 0.1)

veh_xspeed_SFM = np.array(veh_xspeed_SFM)
ped_yspeed_SFM = -np.array(ped_yspeed_SFM)
veh_ped_distance_x_SFM = np.array(veh_ped_distance_x_SFM)
veh_ped_distance_y_SFM = np.array(veh_ped_distance_y_SFM)
time_SFM = np.array(time_SFM)

veh_xspeed_MDP = np.array(veh_xspeed_MDP)
ped_yspeed_MDP = -np.array(ped_yspeed_MDP)
veh_ped_distance_x_MDP = np.array(veh_ped_distance_x_MDP)
veh_ped_distance_y_MDP = np.array(veh_ped_distance_y_MDP)
time_MDP = np.array(time_MDP)

fig, ax = plt.subplots(2, 1, figsize=(30,10))

ax[0].plot(time_SFM, veh_ped_distance_x_SFM, color='red', label='veh_disctance_to_intersection_SFM')
ax[0].plot(time_SFM, veh_ped_distance_y_SFM, color='green', label='ped_disctance_to_intersection_SFM')
ax[0].plot(time_MDP, veh_ped_distance_x_MDP, 'r--', label='veh_disctance_to_intersection_MDP')
ax[0].plot(time_MDP, veh_ped_distance_y_MDP, 'g--', label='ped_disctance_to_intersection_MDP')
ax[0].set_xlabel('time[s]')
ax[0].set_ylabel('distance[m]')
ax[0].legend()
ax[0].set_title('Distance to intersection')
ax[0].grid()

ax[1].plot(time_SFM, veh_xspeed_SFM, color='red', label='veh_xspeed_SFM')
ax[1].plot(time_SFM, ped_yspeed_SFM, color='green', label='ped_yspeed_SFM')
ax[1].plot(time_MDP, veh_xspeed_MDP, 'r--', label='veh_xspeed_MDP')
ax[1].plot(time_MDP, ped_yspeed_MDP, 'g--', label='ped_yspeed_MDP')
ax[1].set_xlabel('ped_distance_from_road[m]')
ax[1].set_ylabel('speed[m/s]')
ax[1].legend()
ax[1].set_title('Speed of Vehicle and Pedestrian')
ax[1].grid()

plt.show()

fig, ax = plt.subplots(1, 1, figsize=(20,10))
ax.plot(time_SFM, veh_ped_distance_x_SFM, color='red', label='distance_SFM')
ax.plot(time_MDP, veh_ped_distance_x_MDP, color='green', label='distance_MDP')
ax.set_xlabel('time[s]')
ax.set_ylabel('distance[m]')
ax.legend()
ax.set_title('Distance between vehicle and pedestrian')
ax.grid()

plt.show()

# tikzplotlib.save("figure.tex")
