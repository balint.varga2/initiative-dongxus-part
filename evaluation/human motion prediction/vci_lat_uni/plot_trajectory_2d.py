import csv
from matplotlib import pyplot as plt
from utils.human_motion.pedestrian_SFM_2d import social_force_model_2d
from utils.human_motion.pedestrian_MDP_2d import predict_ped_with_MDP_2d
from utils.common.sim_model_objects import Object
import numpy as np
PRINT = 1
PED_INDEX = 4
PRED_HORIZON = 1
dt = 0.03

# ------------------------------------------------------------------------

veh_info = list(csv.reader(open('dataset/unidirection_normal_driving_04_traj_veh_filtered.csv')))[1::]
N = len(veh_info)
veh_info = veh_info[::PRED_HORIZON]
ped_info = list(csv.reader(open('dataset/unidirection_normal_driving_04_traj_ped_filtered.csv')))[1 + N * (PED_INDEX-1) : 1 + N * PED_INDEX : PRED_HORIZON]
n = len(ped_info)

x_pred_SFM = [float(ped_info[0][3])]
y_pred_SFM = [float(ped_info[0][4])]
x_pred_MDP = [float(ped_info[0][3])]
y_pred_MDP = [float(ped_info[0][4])]
time = []
x_pred_baseline = []
y_pred_baseline = []

veh_x = []
veh_y = []
ped_x = []
ped_y = []

for i in range(n-1):
    ped = Object()
    ped.x_var = float(ped_info[i][3])
    ped.y_var = float(ped_info[i][4])
    ped.xspeed_ms_var = float(ped_info[i][-2])
    ped.yspeed_ms_var = float(ped_info[i][-1])
    ped.xspeed_ref_para = 1.0
    ped.xspeed_max_para = 1.5
    ped.yspeed_ref_para = 1.4
    ped.yspeed_max_para = 2.0
    ped.intention_var = 1.0
    ped.x_init_var = float(ped_info[0][3])
    ped.y_init_var = float(ped_info[0][4])

    veh = Object()
    veh.x_var = float(veh_info[i][3])
    veh.y_var = float(veh_info[i][4])
    veh.x_init_var = float(veh_info[0][3]) * np.sign(float(veh_info[0][-2]))
    veh.xspeed_ms_var = float(veh_info[i][-1]) * np.sign(float(veh_info[0][-2]))

    pred_speed = social_force_model_2d(ped, veh)
    x_pred_SFM.append(ped.x_var + pred_speed[0] * dt)
    y_pred_SFM.append(ped.y_var + pred_speed[1] * dt)
    # x_pred_SFM.append(x_pred_SFM[-1] + pred_speed[0] * dt)
    # y_pred_SFM.append(y_pred_SFM[-1] + pred_speed[1] * dt)

    pred_speed = predict_ped_with_MDP_2d(ped, veh)
    x_pred_MDP.append(ped.x_var + pred_speed[0] * dt)
    y_pred_MDP.append(ped.y_var + pred_speed[1] * dt)
    # x_pred_MDP.append(x_pred_MDP[-1] + pred_speed[0] * dt)
    # y_pred_MDP.append(y_pred_MDP[-1] + pred_speed[1] * dt)

    x_pred_baseline.append(ped.x_var + ped.xspeed_ms_var * dt)
    y_pred_baseline.append(ped.y_var + ped.yspeed_ms_var * dt)

    time.append(i * 0.1)

    ped_x.append(float(ped_info[i][3]))
    ped_y.append(float(ped_info[i][4]))
    veh_x.append(float(veh_info[i][3]))
    veh_y.append(float(veh_info[i][4]))

# x_pred_SFM = x_pred_SFM[:-1]
# y_pred_SFM = y_pred_SFM[:-1]
# x_pred_MDP = x_pred_MDP[:-1]
# y_pred_MDP = y_pred_MDP[:-1]
# x_pred_baseline = [ped_x[0]] + x_pred_baseline[:-1]
# y_pred_baseline = [ped_y[0]] + y_pred_baseline[:-1]

if PRINT:
    # fig, ax = plt.subplots(1, 1)
    # ax.plot(ped_x, ped_y, color='red', label='pedestrian')
    # ax.plot(veh_x, veh_y, color='green', label='vehicle')
    # ax.set_xlabel('x[m]')
    # ax.set_ylabel('y[m]')
    # ax.set_title('Scenario')
    # ax.legend()
    # plt.show()

    plt.scatter(ped_x, ped_y, c=time, cmap='coolwarm', alpha=0.8)
    plt.scatter(veh_x, veh_y, c=time, cmap='coolwarm', alpha=0.8)
    plt.xlabel('x in m')
    plt.ylabel('y in m')
    cbar = plt.colorbar()
    cbar.set_label('Time in s')
    plt.title("Scenario")
    plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.plot(ped_x, ped_y, color='black', label='pedestrian_original')
    ax.plot(x_pred_SFM, y_pred_SFM, color='red', label='SFM')
    ax.plot(x_pred_MDP, y_pred_MDP, color='green', label='MDP')
    # ax.plot(x_pred_baseline, y_pred_baseline, 'b--', label='baseline')
    ax.set_xlabel('x in m')
    ax.set_ylabel('y in m')
    ax.set_title('Human Prediction Model')
    ax.legend()
    plt.show()




