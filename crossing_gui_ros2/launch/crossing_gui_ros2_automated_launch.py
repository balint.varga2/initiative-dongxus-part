"""Example Launch file
You can do the followings:
- Set which config file to use. (parameter set)
- Define the nodes you want to use.
- At the end, decide which nodes to start.
"""

# Import YAML config files:
import os
from ament_index_python.packages import get_package_share_directory

# Launch description generation:
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    ld = LaunchDescription()

    # Path to the YAML config file:
    config = os.path.join(
        get_package_share_directory('crossing_gui_ros2'),
        'config',
        'dynamic_negotiation_config.yaml'
        )

    control_vehicle_node = Node(
        package='crossing_gui_ros2',
        executable='control_vehicle',
        name='control_vehicle_node',
        parameters=[config]
    )

    control_pedestrian_node = Node(
        package='crossing_gui_ros2',
        executable='control_pedestrian',
        name='control_pedestrian_node',
        parameters=[config]
    )

    pedestrian_crossing_simulation_node = Node(
        package='crossing_gui_ros2',
        executable='pedestrian_crossing_simulation',
        name='pedestrian_crossing_simulation_node',
        parameters=[config]
    )

    log_node = Node(
        package='crossing_gui_ros2',
        executable='log',
        name='log_node',
        parameters=[config]
    )
    
    ld.add_action(control_vehicle_node)
    ld.add_action(control_pedestrian_node)
    ld.add_action(pedestrian_crossing_simulation_node)
    #ld.add_action(log_node)

    # The launch file returns a LaunchDescription object:
    return ld

    # RUN COMMAND: ros2 launch crossing_gui_ros2 crossing_gui_ros2_automated_launch.py
