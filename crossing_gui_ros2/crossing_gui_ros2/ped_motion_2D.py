#!/usr/bin/env python3
"""
    This node uses Annikas "social force model 2" to control the pedestrian object in the gui.
"""

"""
TODOS:
    - Parameter descriptor: floating_point_range=set(my_floating_point_range)
        could not figure out how to use this, but it is not important, maybe in the future.

    To implement the social force model 2:
    - Lets understand annikas code becouse it has different coordinates
    - initialize the database

"""

# ================= Imports: =================

# ROS2:
import rclpy
from rclpy.node import Node

# ROS2 message definitions:
from crossing_gui_ros2_interfaces.msg import Pedestriancontrol
from crossing_gui_ros2_interfaces.msg import GuiFeedback
from utils.common.sim_model_objects import Object
from utils.human_motion.pedestrian_SFM_2d import social_force_model_2d
from utils.human_motion.pedestrian_MDP_2d import predict_ped_with_MDP_2d

TIMER_PERIOD = 0.1
human_model_ls = {'SFM': social_force_model_2d,
                  'MDP': predict_ped_with_MDP_2d}


class PedestrianMotionNode(Node):
    """
    The Node for publishing the pedestrian control message.
    """
    def __init__(self):
        super().__init__('ped_motion_2D')

        # define parameters
        self.declare_parameters(
            namespace='',
            parameters=[
                ('human_model', 'SFM'),
                ('cross', True),
                ('ped_intention', 1.0)
            ])

        self.veh = Object()

        self.ped = Object()
        self.ped.intention_var = 0.8
        self.ped.xspeed_ref_para = 1.0
        self.ped.xspeed_max_para = 1.5
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.5

        self.ped.intention_var = self.get_parameter('ped_intention').value
        self.human_model = self.get_parameter('human_model').value

        # Create timer for publishing:
        self.timer = self.create_timer(TIMER_PERIOD, self.timer_callback)

        # Publishers:
        self.publisher_pedestrian_control = self.create_publisher(Pedestriancontrol,
                                                   'simulation/input/pedestrian', 10)

        # Subscribers:
        self.subscriber_gui_feedback = self.create_subscription(
            GuiFeedback,
            'simulation/output/gui_feedback',
            self.callback_gui_feedback,
            10)
        self.subscriber_gui_feedback  # prevent unused variable warning

        # Create the pedestrian message object:
        self.pedestrian_control_message = Pedestriancontrol()

        # END OF INIT

    def callback_gui_feedback(self, data):
        """Callback function for the 'simulation/output/gui_feedback' subscriber.
        We need the position and the velocities of the pedestrian and vehicle objects!
        """

        self.ped.x_var = data.pedestrian_x
        self.ped.y_var = data.pedestrian_y
        self.ped.xspeed_ms_var = data.pedestrian_x_speed
        self.ped.yspeed_ms_var = data.pedestrian_y_speed
        self.ped.x_init_var = data.pedestrian_x_init
        self.ped.y_init_var = data.pedestrian_y_init

        self.veh.x_var = data.vehicle_x
        self.veh.y_var = data.vehicle_y
        self.veh.xspeed_ms_var = data.vehicle_x_speed
        self.veh.x_init_var = data.vehicle_x_init

    def timer_callback(self):

        # update parameters
        self.ped.intention_var = self.get_parameter('ped_intention').value
        self.human_model = self.get_parameter('human_model').value
        cross = self.get_parameter('cross').value

        # select prediction model
        human_model = human_model_ls[self.human_model]

        # calculate speed at next time step
        pedestrian_speed_new = human_model(self.ped, self.veh, cross=cross)

        self.pedestrian_control_message.pedestrian_x_speed = pedestrian_speed_new[0]
        self.pedestrian_control_message.pedestrian_y_speed = pedestrian_speed_new[1]
        self.pedestrian_control_message.pedestrian_intention = self.ped.intention_var
        self.pedestrian_control_message.pedestrian_displaymessage = "crossing" if self.pedestrian_control_message.pedestrian_intention >= 0.5 else "chilling"

        # Publish the message:
        self.publisher_pedestrian_control.publish(self.pedestrian_control_message)


def main(args=None):
    rclpy.init(args=args)

    pedestrian_motion_node = PedestrianMotionNode()

    rclpy.spin(pedestrian_motion_node, executor=None)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    pedestrian_motion_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()