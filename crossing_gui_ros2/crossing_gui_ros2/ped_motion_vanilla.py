import rclpy
from rclpy.node import Node
from crossing_gui_ros2_interfaces.msg import GuiFeedback
from crossing_gui_ros2_interfaces.msg import Pedestriancontrol


class TestPedestrianMotion(Node):

    def __init__(self):
        super().__init__('test_pedestrian_motion')

        self.ped_motion_cmd = Pedestriancontrol()
        self.ped_motion_cmd.pedestrian_x_speed = 0.0

        self.ped_distance_from_road = 0.0
        self.initial_distance_from_road = 10.0
        self.ped_y = 0.0
        self.ped_y_init = 0.0
        self.ped_y_speed = 0.0
        self.ped_x = 0.0
        self.veh_x = 0.0
        self.veh_xspeed = 0.0

        self.count = 0

        self.model_state_subscription = self.create_subscription(
            GuiFeedback,
            '/simulation/output/gui_feedback',
            self.model_state_callback, 10)
        self.model_state_subscription

        self.publisher_pedestrian_control = self.create_publisher(Pedestriancontrol,
                                                                  'simulation/input/pedestrian', 10)
        timer_period = 0.01  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def model_state_callback(self, model_state):
        veh_x_threshold = 89.9
        veh_arrives_end = self.veh_x < veh_x_threshold and model_state.vehicle_x >= veh_x_threshold
        if veh_arrives_end:
            self.count += 1
        self.veh_x = model_state.vehicle_x
        self.veh_xspeed = model_state.vehicle_x_speed

        self.ped_y = model_state.pedestrian_y
        self.ped_y_init = model_state.pedestrian_y_init
        self.ped_y_speed = model_state.pedestrian_y_speed
        self.ped_x = model_state.pedestrian_x
        self.ped_distance_from_road = self.ped_y - model_state.vehicle_y  # y coordinate of center of road is 10.0
        self.initial_distance_from_road = model_state.pedestrian_y_init - model_state.vehicle_y

    def set_ped_motion_case1(self):
        self.ped_motion_cmd.pedestrian_y_speed = - 1.4
        self.ped_motion_cmd.pedestrian_intention = 0.3
        self.ped_motion_cmd.pedestrian_displaymessage = 'thinking'

    def set_ped_motion_case2(self):
        if self.ped_distance_from_road >= 5.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.8
            self.ped_motion_cmd.pedestrian_intention = 1.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'crossing'
        elif 3.0 < self.ped_distance_from_road < 5.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.8 * (self.ped_distance_from_road - 3.0) / (5.0 - 3.0)
            self.ped_motion_cmd.pedestrian_intention = 0.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'stopping'
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.8
            self.ped_motion_cmd.pedestrian_intention = 1.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'crossing'

    def set_ped_motion_case3(self):
        if abs(self.veh_x - self.ped_x) >= 8.0:
            self.ped_motion_cmd.pedestrian_y_speed = 0.0
            self.ped_motion_cmd.pedestrian_intention = 0.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'stopping'
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.8
            self.ped_motion_cmd.pedestrian_intention = 1.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'crossing'

    def set_ped_motion_case4(self):
        if self.ped_distance_from_road >= 5.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.5
            self.ped_motion_cmd.pedestrian_intention = 0.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'thinking'
        if 3.0 < self.ped_distance_from_road < 5.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.7
            self.ped_motion_cmd.pedestrian_intention = 1.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'crossing'
        if 0.0 < self.ped_distance_from_road <= 3.0:
            self.ped_motion_cmd.pedestrian_intention = 1.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'crossing'
            if self.veh_xspeed > 1.0:
                self.ped_motion_cmd.pedestrian_y_speed = 0.0
            else:
                self.ped_motion_cmd.pedestrian_y_speed = - 1.5
        if self.ped_distance_from_road <= 0.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.5
            self.ped_motion_cmd.pedestrian_intention = 0.0
            self.ped_motion_cmd.pedestrian_displaymessage = 'thinking'

    def set_ped_motion_case5(self):
        if self.ped_distance_from_road >= 3.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.0
            self.ped_motion_cmd.pedestrian_intention = 0.5
            self.ped_motion_cmd.pedestrian_displaymessage = "thinking"
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 0.5
            self.ped_motion_cmd.pedestrian_intention = 0.2
            self.ped_motion_cmd.pedestrian_displaymessage = "stopping"

    def set_ped_motion_case6(self):
        if self.ped_distance_from_road >= 3.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.0
            self.ped_motion_cmd.pedestrian_intention = 0.5
            self.ped_motion_cmd.pedestrian_displaymessage = "thinking"
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.5
            self.ped_motion_cmd.pedestrian_intention = 0.8
            self.ped_motion_cmd.pedestrian_displaymessage = "crossing"

    def set_ped_motion_case7(self):
        if self.ped_distance_from_road > 0:
            self.ped_motion_cmd.pedestrian_y_speed = - 0.3 - 1.2 * abs(self.ped_distance_from_road - self.initial_distance_from_road) / self.initial_distance_from_road
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.5
        self.ped_motion_cmd.pedestrian_intention = min(1.0, 1.0 * abs(self.ped_distance_from_road - self.initial_distance_from_road) / self.initial_distance_from_road)
        self.ped_motion_cmd.pedestrian_displaymessage = "thinking" if self.ped_motion_cmd.pedestrian_intention <= 0.6 else "crossing"

    def set_ped_motion_case8(self):
        self.ped_motion_cmd.pedestrian_y_speed = - 0.5 - 1.0 * self.ped_distance_from_road / self.initial_distance_from_road
        self.ped_motion_cmd.pedestrian_intention = 1.0 * self.ped_distance_from_road / self.initial_distance_from_road
        self.ped_motion_cmd.pedestrian_displaymessage = "thinking" if self.ped_motion_cmd.pedestrian_intention <= 0.6 else "crossing"

    def set_ped_motion_case9(self):
        if self.ped_distance_from_road > 0.0:
            self.ped_motion_cmd.pedestrian_y_speed = - 0.7 - 0.3 * abs(self.ped_distance_from_road - self.initial_distance_from_road) / self.initial_distance_from_road
        else:
            self.ped_motion_cmd.pedestrian_y_speed = - 1.0
        self.ped_motion_cmd.pedestrian_intention = 0.8
        self.ped_motion_cmd.pedestrian_displaymessage = "crossing"

    def timer_callback(self):

        # if self.count % 8 == 0:
        #     self.set_ped_motion_case1()
        # elif self.count % 8 == 1:
        #     self.set_ped_motion_case2()
        # elif self.count % 8 == 2:
        #     self.set_ped_motion_case3()
        # elif self.count % 8 == 3:
        #     self.set_ped_motion_case4()
        # elif self.count % 8 == 4:
        #     self.set_ped_motion_case5()
        # elif self.count % 8 == 5:
        #     self.set_ped_motion_case6()
        # elif self.count % 8 == 6:
        #     self.set_ped_motion_case7()
        # else:
        #     self.set_ped_motion_case8()

        self.set_ped_motion_case7()

        # self.get_logger().info("The current test case is case%d" % (self.count % 8 + 1))

        # self.set_ped_motion_case8()
        self.publisher_pedestrian_control.publish(self.ped_motion_cmd)


def main(args=None):
    rclpy.init(args=args)

    test_pedestrian_motion = TestPedestrianMotion()

    rclpy.spin(test_pedestrian_motion)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)

    test_pedestrian_motion.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()