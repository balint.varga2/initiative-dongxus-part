#!/usr/bin/env python3
"""
    Node for controlling the pedestrian object.
    This node publishes the speed information for the pedestrian object in the GUI.
"""

"""
TODOS:
    - Parameter descriptor: floating_point_range=set(my_floating_point_range)
        could not figure out how to use this, but it is not important, maybe in the future.
"""

# ================= Imports: =================
# Python:
import sys
import os
import pathlib

# Pygame:
import pygame

# ROS2:
import rclpy
from rclpy.node import Node
from rcl_interfaces.msg import ParameterDescriptor # for declaring parameters

# ROS2 message and service definitions:
from crossing_gui_ros2_interfaces.msg import Pedestriancontrol
from crossing_gui_ros2_interfaces.msg import GuiFeedback
from crossing_gui_ros2_interfaces.srv import SimulationInfos

# ================= Constants: =================
# Size of the speedchange on button push:
DELTASPEED = 0.2 # m/s

# Colors:
BLACK         = (0,0,0)
WHITE         = (255, 255, 255)
BRIGHTBLUE    = (0,50,255)
DARKTURQUOISE = (3,54,73)
GREEN         = (0,204,0)

# Text:
FONTNAME            = 'freesansbold.ttf'
TEXTCOLOR           = BLACK
BASICFONTSIZE       = 18
TEXTBACKGROUNDCOLOR = WHITE

# Period of publishing the message:
TIMER_PERIOD = 1/30 # seconds (~ 30 fps)

# Asset folder setup:
PARENTDIR = pathlib.Path(__file__).parent
FONTSDIR = os.path.join(PARENTDIR, 'fonts')

class PedestrianControlMessage():
    """
    An object for the pedestrian control message.

    Attributes:
        msg (Pedestriancontrol): Pedestrian control custom message.
    """
    def __init__(self):
        self.msg = Pedestriancontrol()

        # Set default values:
        self.msg.pedestrian_x_speed = 0.0 # [m/s]
        self.msg.pedestrian_y_speed = 0.0 # [m/s]
        self.msg.pedestrian_displaymessage = 'stopping' # crossing, stopping
        self.msg.pedestrian_intention = 1.0 # between 0 and 1

    # Get the message data:
    def getxspeed(self):            return self.msg.pedestrian_x_speed
    def getyspeed(self):            return self.msg.pedestrian_y_speed
    def getdisplaymessage(self):    return self.msg.pedestrian_displaymessage
    def getintention(self):         return self.msg.pedestrian_intention

    # Set the new message data:
    def setxspeed(self, x_speed_new):        self.msg.pedestrian_x_speed = x_speed_new
    def setyspeed(self, y_speed_new):        self.msg.pedestrian_y_speed = y_speed_new
    def setdisplaymessage(self, message_new):self.msg.pedestrian_displaymessage = message_new
    def setintention(self, intention_new):   self.msg.pedestrian_intention = intention_new


class PedestrianControlNode(Node):
    """
    The Node for publishing the pedestrian control message.

    Args:

    Attributes:
        deltaspeed (double): speedchange upon button press
    """

    def __init__(self):
        super().__init__('pedestrian_control_node')

        # ========== Initialize ROS2 things ==========

        # Parameter descriptors:
        descriptor_getlogger_enable_pedcontrol = ParameterDescriptor(
             name='getlogger_enable_pedcontrol',
             type=1,  # 1:bool, 3:double
             description='Enables the get_logger() function to post infos in the terminal.',
             additional_constraints='boolean: True or False',
             read_only=False,
             dynamic_typing=False,
         )

        # Parameter declarations:
        self.declare_parameter(name = 'getlogger_enable_pedcontrol',
                               value = True, # default value
                               descriptor = descriptor_getlogger_enable_pedcontrol)

        # Read parameters from parameter server:
        self.getlogger_enable_pedcontrol = self.get_parameter('getlogger_enable_pedcontrol').value

        # Create timer for publishing:
        self.timer = self.create_timer(TIMER_PERIOD, self.timer_callback)

        # Create publishers:
        self.publisher_pedestrian_control = self.create_publisher(Pedestriancontrol,
                                                   'simulation/input/pedestrian', 10)

        # Create subscribers:
        self.subscriber_gui_feedback = self.create_subscription(
            GuiFeedback,
            'simulation/output/gui_feedback',
            self.callback_gui_feedback,
            10)
        self.subscriber_gui_feedback  # prevent unused variable warning

        # Services:
        # ---

        # Service clients:
        self.client_general_infos = self.create_client(SimulationInfos, 'simulation/general_infos')
        # This service provides initial information about the gui, e.g. the initial
        # position of the vehicle.
        while not self.client_general_infos.wait_for_service(timeout_sec=1.0):
            # Wait here until service is available.
            self.get_logger().info("Waiting for service 'simulation/general_infos' ... ")

        # Create the request object:
        self.req = SimulationInfos.Request()
        # (Since the service does not require any parameters, the request object is empty.)

        # Service call with request object:
        self.future = self.client_general_infos.call_async(self.req)

        # Initialize some other variables:
        # Wait for the initial service response:
        self.waiting_for_data = True

        # Create the pedestrian message object:
        self.pedestrian_control_message = PedestrianControlMessage()

        # ========== Initialize PYGAME things: ==========
        pygame.init()
        self.display_surf = pygame.display.set_mode((500,300))
        pygame.display.set_caption('Pedestrian control')
        self.basicfont = pygame.font.Font(os.path.join(FONTSDIR, FONTNAME), BASICFONTSIZE)

        # Set background color:
        self.display_surf.fill(WHITE)
        # Draw some informations on the window:
        TEXT_SURF, TEXT_RECT = self.maketext('UP: increment speed',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, 10)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('DOWN: decrement speed',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('displaymessage stopping: s',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('displaymessage crossing: c',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('1: 1.0 [m/s]',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('2: 1.5 [m/s]',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('3: 2.0 [m/s]',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('SPACE: 0 [m/s]',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('q: 0.0 intention value',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('w: 0.2 intention value',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('e: 0.5 intention value',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('r: 0.8 intention value',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)
        TEXT_SURF, TEXT_RECT = self.maketext('t: 1.0 intention value',TEXTCOLOR, TEXTBACKGROUNDCOLOR, 10, TEXT_RECT.bottom)
        self.display_surf.blit(TEXT_SURF, TEXT_RECT)


        # ========== Initialize OTHER things: ==========
        # Size of the speedchange on button push:
        self.deltaspeed = DELTASPEED

        # Since displayed information is static, only flip once:
        pygame.display.flip()


        self.first_keydown_flag = False
        self.service_response = None
        self.running_flag = True
        self.pedestrian_y = 0.0

        # set constraint
        self.ped_yspeed_max = 2.5

        # END OF INIT

    # Parameter manipulation:
    def getdeltaspeed(self):        return self.deltaspeed

    def callback_gui_feedback(self, data):
        """Callback function for the 'simulation/output/gui_feedback' subscriber.
        """

        # If pedestrian is on the border, set speed to zero:
        if data.isonborder == True:
            self.pedestrian_control_message.setxspeed(0.0)
            self.pedestrian_control_message.setyspeed(0.0)

        self.pedestrian_y = data.pedestrian_y

    def on_event(self, ped_control_msg, deltaspeed):
        """Event handling.

        Args:
            ped_control_msg (PedestrianControlMessage): class for message object
                ped_control_msg.msg (Pedestriancontrol): Custom message for pedestrian control
                    ped_control_msg.msg.xspeed (double): Speed in x direction
                    ped_control_msg.msg.yspeed (double): Speed in y direction
                    ped_control_msg.msg.displaymessage (string): Info message
            deltaspeed (double): speedchange upon button press
        """

        # Quit in case of closing the window:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running_flag = False

        # Old values:
        x_speed_old = ped_control_msg.getxspeed()
        y_speed_old = ped_control_msg.getyspeed()
        displaymessage_old = ped_control_msg.getdisplaymessage()
        intention_old = ped_control_msg.getintention()
        # New values:
        x_speed_new = x_speed_old
        y_speed_new = y_speed_old
        displaymessage_new = displaymessage_old
        intention_new = intention_old

        # Load information about all keys:
        keys = pygame.key.get_pressed()

        # Pressed key events:
        if keys[pygame.K_UP] and not keys[pygame.K_DOWN]:
            y_speed_new -= deltaspeed
            y_speed_new = max(y_speed_new, -self.ped_yspeed_max)
            self.first_keydown_flag = True
        elif keys[pygame.K_DOWN] and not keys[pygame.K_UP]:
            y_speed_new += deltaspeed
            y_speed_new = min(y_speed_new, self.ped_yspeed_max)
            self.first_keydown_flag = True
        elif keys[pygame.K_ESCAPE]:
            self.running_flag = False
        elif keys[pygame.K_s] and not keys[pygame.K_c]:
            displaymessage_new = 'stopping'
        elif not keys[pygame.K_s] and keys[pygame.K_c]:
            displaymessage_new = 'crossing'
        elif keys[pygame.K_1]:
            y_speed_new = -1.0
        elif keys[pygame.K_2]:
            y_speed_new = -1.5
        elif keys[pygame.K_3]:
            y_speed_new = -2.0
        elif keys[pygame.K_SPACE]:
            x_speed_new = 0.0
            y_speed_new = 0.0
        elif keys[pygame.K_q]:
            intention_new = 0.0
        elif keys[pygame.K_w]:
            intention_new = 0.2
        elif keys[pygame.K_e]:
            intention_new = 0.5
        elif keys[pygame.K_r]:
            intention_new = 0.8
        elif keys[pygame.K_t]:
            intention_new = 1.0

        ped_control_msg.setxspeed(x_speed_new)
        ped_control_msg.setyspeed(y_speed_new)
        ped_control_msg.setdisplaymessage(displaymessage_new)

        ped_control_msg.setintention(intention_new)

        return ped_control_msg

    def maketext(self, text, color, bgcolor, top, left):
        # Create the Surface and Rect objects for some text:
        textsurf = self.basicfont.render(text, True, color, bgcolor)
        textrect = textsurf.get_rect()
        textrect.topleft = (top, left)
        return (textsurf, textrect)

    def timer_callback(self):
        """
        Check for events and publish them.
        """

        if self.waiting_for_data == True:
            # Check whether future object is ready:
            if self.future.done():
                try:
                    # This block will test the excepted error to occur.
                    # Try to extract the response:
                    response = self.future.result()
                except Exception as e:
                    # Here you can handle the error.
                    # If Service request failed, print out error:
                    self.get_logger().info(
                        'Service call failed %r' % (e,))
                else:
                    # If there is no exception then this block will be executed.
                    # If service request was succesful:
                    # Print some confirmation in the terminal:
                    if self.getlogger_enable_pedcontrol:
                        self.get_logger().info(
                            f"Some of the arrived data: \n \
                                WIDTH = {response.display_width} \n \
                                HEIGHT = {response.display_height}")

                    # Update the waiting flag:
                    self.waiting_for_data = False

                    # Save the data
                    self.service_response = response
                    self.pedestrian_y = self.service_response.pedestrian_y

                finally:
                    # Finally block always gets executed either exception is generated or not.
                    pass
            else:
                print("Waiting for service response ...")

# --------------------------------------------------------------------------------------------------
        else:
            if self.running_flag == True:

                # Event handling function:
                self.pedestrian_control_message = self.on_event(self.pedestrian_control_message, self.deltaspeed)

                if self.first_keydown_flag == False:
                    # desired_distance_from_road = 3.0
                    # walking_speed = -1.4
                    # road_y = self.service_response.road_y
                    #
                    # if self.pedestrian_y > road_y + desired_distance_from_road:
                    #     self.pedestrian_control_message.setyspeed(walking_speed)
                    # else:
                    #     self.pedestrian_control_message.setyspeed(0.0)

                    self.pedestrian_control_message.setyspeed(0.0)

                # Publish the message into 'Move_Pedestrian_with_arrows' topic:
                self.publisher_pedestrian_control.publish(self.pedestrian_control_message.msg)

                # Log the message into the command line:
                if self.getlogger_enable_pedcontrol:
                    logstring = f"SEND MSG: {self.pedestrian_control_message.getxspeed()},\
                                            {self.pedestrian_control_message.getyspeed()},\
                                            {self.pedestrian_control_message.getdisplaymessage()},\
                                            {self.pedestrian_control_message.getintention()}"
                    self.get_logger().info(logstring)
            else:
                # Stop the node if request is present:
                print("Shutting down")
                pygame.quit()
                sys.exit()

# --------------------------------------------------------------------------------------------------

def main(args=None):
    rclpy.init(args=args)

    pedestrian_control_node = PedestrianControlNode()

    rclpy.spin(pedestrian_control_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    pedestrian_control_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()