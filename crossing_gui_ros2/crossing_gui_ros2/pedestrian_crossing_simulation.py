#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# If you use launch files, you have to build the package again!
# Command: colcon build --packages-select crossing_gui_ros2

# To check for missing dependencies, run following command in the root of your workspace:
# rosdep install -i --from-path src --rosdistro foxy -y

"""TODOS:
- only show area of pedestriant crossing if road is too long
"""

# ============ SETUP: ============

# Do you want to generate matlab log-files?
LOG = False


# ============ IMPORTS: ============
# Import modules:
import sys
import time
from turtle import window_height
import pygame
import os
import math
import numpy as np
import scipy.io

# Import the file "create_parameters.py" which contains the "create_parameters" function:
# (Build the package first, otherwise it won't work, I have no idea why.)
from crossing_gui_ros2.simulation_node_functions import create_parameters

# Import ROS2:
import rclpy
from rclpy.node import Node
from rcl_interfaces.msg import ParameterDescriptor # for declaring parameters
from rcl_interfaces.msg import IntegerRange
from rcl_interfaces.msg import FloatingPointRange

# Import ROS2 message definitions:
from crossing_gui_ros2_interfaces.msg import Vehiclecontrol
from crossing_gui_ros2_interfaces.msg import Pedestriancontrol
from crossing_gui_ros2_interfaces.msg import GuiFeedback
from crossing_gui_ros2_interfaces.msg import Log

# Import ROS2 service definitions:
from crossing_gui_ros2_interfaces.srv import SimulationInfos
from std_srvs.srv import Empty
from sensor_msgs.msg import Joy

# ============ CONSTANTS: ============
# Color constants:
#            R    G    B
GRAY     = (100, 100, 100)
NAVYBLUE = ( 60,  60, 100)
WHITE    = (255, 255, 255)
RED      = (255,   0,   0)
GREEN    = (  0, 255,   0)
DARKGREEN= (  0, 100,   0)
BLUE     = (  0,   0, 255)
YELLOW   = (255, 255,   0)
ORANGE   = (255, 128,   0)
PURPLE   = (255,   0, 255)
CYAN     = (  0, 255, 255)
BLACK    = (  0,   0,   0)
BACKGROUNDCOLOR = GREEN

# Speed of the simulation:
FPS = 100

# Asset folder setup:
SCRIPTFOLDER = os.path.dirname(os.path.realpath(__file__))
IMGSFOLDER = os.path.join(SCRIPTFOLDER, 'imgs')
FONTSFOLDER = os.path.join(SCRIPTFOLDER, 'fonts')

# Text constants:
FONTNAME = 'freesansbold.ttf'
TEXTCOLOR = BLACK
BASICFONTSIZE = 20
pygame.font.init()
FONT = pygame.font.Font(os.path.join(FONTSFOLDER, FONTNAME), BASICFONTSIZE)
TEXTBACKGROUNDCOLOR = None # (Background is set to transparent)

# ============ SPRITE OBJECTS: ============
class Object(pygame.sprite.Sprite):
    """Class for moving objects on the screen.
    Currently there are 2 objects, the vehicle and the pedestrian.
    """

    def __init__(self, image_name, scaling_factor = 1, pixel_per_meter_input = 20):
        """Load the image, scale it and position it according to the coordinates.

        Args:
            image_name (_type_): Filename of an image for the object.
            scaling_factor (int, optional): Proportionality factor to scale image. Defaults to 1.
        """
        self.pixel_per_meter = pixel_per_meter_input
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(IMGSFOLDER, image_name)).convert()
        self.image = self.scaling(self.image, scaling_factor)
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.xspeed = 0.0
        self.yspeed = 0.0
        self.x = 5.0
        self.y = 5.0
        self.displaymessage = "-"
        self.intention = 0.5
        self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter) # in pixel
        self.onborder = False

    def getx(self):             return self.x
    def gety(self):             return self.y
    def getdisplaymessage(self):return self.displaymessage
    def getxspeed(self):        return self.xspeed
    def getyspeed(self):        return self.yspeed
    def getonborder(self):      return self.onborder
    def getintention(self):     return self.intention

    def setx(self, x):
        self.x = x; self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter)
    def sety(self, y):
        self.y = y; self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter)
    def setdisplaymessage(self, displaymessage):  self.displaymessage = displaymessage
    def setxspeed(self, speed): self.xspeed = speed
    def setyspeed(self, speed): self.yspeed = speed
    def setonborder(self, onborder): self.onborder = onborder
    def setintention(self, intention): self.intention = intention

    # Scale the loaded image (mainly for car because it was too big on screen):
    def scaling(self,image, scaling_factor):
        rect = image.get_rect()
        original_width  = rect.width
        original_height = rect.height
        scaled_width  = original_width  * scaling_factor
        scaled_height = original_height * scaling_factor
        scaled_image = pygame.transform.scale(image, (scaled_width, scaled_height))
        return scaled_image

    # Draw text next to the object:
    def draw_displaymessage(self, surface):
        textsurf = FONT.render(self.displaymessage + ' ' + str(self.intention), True, TEXTCOLOR, TEXTBACKGROUNDCOLOR)
        textrect = textsurf.get_rect()
        textrect.topleft = (self.rect.bottomright)
        surface.blit(textsurf, textrect)

# ============ MAIN PROGRAM CLASS: ============

class SimulationGuiNode(Node):
    """Main program class.

    Args:
        Node (_type_): Native ROS2 object
    """

    def __init__(self):
        super().__init__('pedestrian_crossing_simulation_node')
        self.running = True

        # Create parameters:
        #self.create_parameters()
        create_parameters.create_parameters(self.declare_parameter, self.get_parameter)

        # Get parameters from ROS2 parameter server:
        self.getlogger_enable_gui = self.get_parameter('getlogger_enable_gui').value

        # Initialize fps:
        timer_period = 1/100 # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

        # Initialize window:
        self.display_width = self.get_parameter('window_width').value
        self.display_height = self.get_parameter('window_height').value
        self.pixel_per_meter = 1
        self.pixel_per_meter = self.get_parameter('pixel_per_meter').value
        self.size = (self.display_width, self.display_height)


        # Initialize pygame:
        pygame.init()
        self.display_surface = pygame.display.set_mode(self.size)
        self.fps_clock = pygame.time.Clock() # not used
        pygame.display.set_caption('Pedestrian Crossing Simulation')

        # Subscribers:
        self.is_joy_control = False
        self.subs_pedestrain_joy_cont = self.create_subscription(
            Joy, '/joy', self.ped_joy_control, 10)
        self.subs_pedestrain_joy_cont  # prevent unused variable warning

        self.subscriber_vehicle_control = self.create_subscription(Vehiclecontrol,
                                                                   'simulation/input/vehicle',
                                                                   self.callback_vehicle,10)
        self.subscriber_vehicle_control  # prevent unused variable warning
        self.subscriber_pedestrian_control = self.create_subscription(Pedestriancontrol,
                                                                      'simulation/input/pedestrian',
                                                                      self.callback_pedestrian,10)
        self.subscriber_pedestrian_control  # prevent unused variable warning


        # Publishers:
        self.publisher_gui_feedback = self.create_publisher(GuiFeedback, 'simulation/output/gui_feedback', 10)
        self.publisher_log = self.create_publisher(Log, 'simulation/output/log', 10)

        # Services:
        self.service_general_infos = self.create_service(SimulationInfos, 'simulation/general_infos', self.callback_general_infos)

        # Service clients:
        self.client_savelogfiles = self.create_client(Empty, 'savelogfiles')

        # Sprite group: (otherwise sprites will be deleted)
        self.all_objects = pygame.sprite.Group()

        # Pedestrian setup:
        self.pedestrian = Object("redpoint.png", 0.3, self.pixel_per_meter)
        self.pedestrian.setx(self.get_parameter('pedestrian_crossing_position').value) # [m]
        self.pedestrian.sety(self.get_parameter('ped_initial_pos_y').value) # [m]
        self.pedestrian.setdisplaymessage('stopping')

        self.pedestrian.setxspeed(0.0) # [m/s]
        self.pedestrian.setyspeed(0.0) # [m/s]
        self.all_objects.add(self.pedestrian)
        self.pedestrian.setonborder(False)

        # Vehicle setup:
        self.vehicle = Object('vehicle_1.png', 0.33, self.pixel_per_meter)

        self.vehicle.setx(self.get_parameter('veh_initial_pos_x').value) # [m]
        self.vehicle.sety(self.get_parameter('road_y_position').value) # [m]

        self.vehicle.setdisplaymessage('driving')
        self.vehicle.setxspeed(0.0) # [m/s]
        self.vehicle.setyspeed(0.0) # [m/s]
        self.all_objects.add(self.vehicle)

        # Road rectangle setup:
        self.road_color = GRAY
        self.road_lane_width = 3.0
        # Create Rect object and set virtual attributes:
        self.road_rect = pygame.Rect(1,1,1,1) # dummy values
        self.road_rect.height = self.meter2pixel(self.road_lane_width)
        self.road_rect.width = self.display_width
        self.road_rect.centery = self.meter2pixel(self.get_parameter('road_y_position').value)
        self.road_rect.left = 0

        # If the simulation is reset:
        self.is_simulation_reset = False


    def meter2pixel(self, meter):
        return meter*self.pixel_per_meter

    def pixel2meter(self, pixel):
        return pixel/self.pixel_per_meter

    def callback_general_infos(self, request, response):
        """Callback function of the service 'simulation/services/general_infos'.
        """

        if self.getlogger_enable_gui:
            self.get_logger().info("Incoming general infos request.")

        # Current coordinates of the vehicle:
        response.vehicle_x = self.vehicle.getx()
        response.vehicle_y = self.vehicle.gety()

        # Initial speed of the vehicle:
        response.vehicle_x_speed_init = self.get_parameter('vehicle_x_speed_init').get_parameter_value().double_value

        # Current coordinates of the pedestrian:
        response.pedestrian_x = self.pedestrian.getx()
        response.pedestrian_y = self.pedestrian.gety()

        # Vertical position of the roadline:
        response.road_y = self.get_parameter('road_y_position').value

        # Width of the roadline:
        response.road_width = self.road_lane_width

        # Horizontal position of the pedestrian crossing:
        response.pedestrian_crossing_x = self.get_parameter('pedestrian_crossing_position').value

        # Conversion rate between meter and pixel: 1 [meter] = pixel_per_meter [pixel]
        response.pixel_per_meter = float(self.pixel_per_meter)

        # Width of the window:
        response.display_width = float(self.pixel2meter(self.get_parameter('window_width').value))

        # Height of the window:
        response.display_height = float(self.display_height)

        # Vehicle length:
        response.vehicle_length = float(self.get_parameter('vehicle_length').get_parameter_value().double_value)

        return response

    def callback_vehicle(self, data):
        """Update vehicle coordinates."""

        # Log data on command window:
        if self.getlogger_enable_gui:
            msg = f"I heard vehicle coordinates: {data.vehicle_x_speed}, {data.vehicle_y_speed}"
            self.get_logger().info(msg)

        # Update the vehicle object:
        self.vehicle.setxspeed(data.vehicle_x_speed)
        self.vehicle.setyspeed(data.vehicle_y_speed)
        self.vehicle.setdisplaymessage(data.vehicle_displaymessage)

    def callback_pedestrian(self, data):
        """Save incoming messages from the pedestrian publisher.
        """
        if not self.is_joy_control:
        # Log data on command window:
            if self.getlogger_enable_gui:
                msg = f"I heard: {data.pedestrian_x_speed}, {data.pedestrian_y_speed}, {data.pedestrian_displaymessage}"
                self.get_logger().info(msg)

            self.pedestrian.setdisplaymessage(data.pedestrian_displaymessage)
            self.pedestrian.setxspeed(data.pedestrian_x_speed)
            self.pedestrian.setyspeed(data.pedestrian_y_speed)
            self.pedestrian.setintention(data.pedestrian_intention)

    def ped_joy_control(self, data):
        self.is_joy_control = True
        if data.buttons[1]:
            displaymessage = 'I am crossing'
        else:
            displaymessage = 'I am stopping'
        self.pedestrian.setdisplaymessage(displaymessage)

        msg = f"I heard: {data.axes[0]}, {data.axes[1]}, {displaymessage}"
        if self.getlogger_enable_gui:
            self.get_logger().info(msg)

        # gain meaning max 5m/s
        # minus is because of coordinates did not fit
        self.pedestrian.setxspeed(-data.axes[0]*5)
        self.pedestrian.setyspeed(-data.axes[1]*5)


    def move_pedestrian(self):
        """Calculates new coordinates for the pedestrian object."""

        oldx = self.pedestrian.getx()
        oldy = self.pedestrian.gety()
        xspeed = self.pedestrian.getxspeed() # [m/s]
        yspeed = self.pedestrian.getyspeed() # [m/s]
        speedcorr2x = xspeed / FPS # correction
        speedcorr2y = yspeed / FPS # correction
        newx = oldx + speedcorr2x
        newy = oldy + speedcorr2y

        # Set new coordinates if they are inside of the window:
        if self.meter2pixel(newy) > 0 and self.meter2pixel(newy) < self.display_height and \
           self.meter2pixel(newx) > 0 and self.meter2pixel(newx) < self.display_width:
            self.pedestrian.setx(newx)
            self.pedestrian.sety(newy)

            # If we just left the border of the screen, indicate this:
            if self.pedestrian.getonborder() == True:
                self.pedestrian.setonborder(False)
        else:
            # Object does not move, indicate that they reached the border of the window:
            self.pedestrian.setonborder(True)

    def move_vehicle(self):
        """Calculates new coordinates for the vehicle object."""
        oldx = self.vehicle.getx()
        oldy = self.vehicle.gety()
        xspeed = self.vehicle.getxspeed() # [m/s]
        yspeed = self.vehicle.getyspeed() # [m/s]
        speedcorr2x = xspeed / FPS # correction
        speedcorr2y = yspeed / FPS # correction
        newx = oldx + speedcorr2x
        newy = oldy + speedcorr2y

        # Set new coordinates if they are inside of the window:
        if self.meter2pixel(newy) > 0 and self.meter2pixel(newy) < self.display_height and \
           self.meter2pixel(newx) > 0 and self.meter2pixel(newx) < self.display_width:
            self.vehicle.setx(newx)
            self.vehicle.sety(newy)
            self.is_simulation_reset = False
        else:
            # Reset the state of vehicle and pedestrian
            self.reset_model_state()

    def reset_model_state(self):

        self.pedestrian.setx(self.get_parameter('pedestrian_crossing_position').value) # [m]
        self.pedestrian.sety(self.get_parameter('ped_initial_pos_y').value) # [m]
        self.pedestrian.setyspeed(0.0)
        self.vehicle.setx(self.get_parameter('veh_initial_pos_x').value)
        self.vehicle.sety(self.get_parameter('road_y_position').value)
        self.is_simulation_reset = True
        self.pedestrian.setonborder(False)
        self.pedestrian.intention = -1.0

    def timer_callback(self):
        """Main program."""

        # Time since epoch in seconds as float:
        tic = time.time()

        # Event handling:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    self.running = False

        # Update Objects:
        self.move_pedestrian()
        self.move_vehicle()

        # Drawing: ============================================================================
        self.display_surface.fill(BACKGROUNDCOLOR)

        # Draw Background Objects:
        self.draw_background_objects()

        # Draw Objects:
        self.all_objects.draw(self.display_surface)

        # Draw Distances:
        # (Returns absolute distance between vehicle and pedestrian.)
        adist = 1
        # self.draw_distances()

        # Draw intentions:
        # self.pedestrian.draw_displaymessage(self.display_surface)
        # self.vehicle.draw_displaymessage(self.display_surface)


        # End of drawing: =====================================================================

        # Blit drawings onto the display:
        pygame.display.flip()

        # After all the stuff, let's save the time and the pedestrian coordinates:
        self.fps_clock.tick(FPS)

        # Elapsed time in one loop
        toc = time.time() - tic

        # Publishing data:
        gui_feedback_msg = GuiFeedback()
        gui_feedback_msg.pedestrian_x = self.pedestrian.getx()
        gui_feedback_msg.pedestrian_y = self.pedestrian.gety()
        gui_feedback_msg.pedestrian_x_speed = self.pedestrian.getxspeed()
        gui_feedback_msg.pedestrian_y_speed = self.pedestrian.getyspeed()
        gui_feedback_msg.pedestrian_displaymessage = self.pedestrian.getdisplaymessage()
        gui_feedback_msg.pedestrian_intention = self.pedestrian.getintention()
        gui_feedback_msg.pedestrian_x_init = self.get_parameter('pedestrian_crossing_position').value
        gui_feedback_msg.pedestrian_y_init = self.get_parameter('ped_initial_pos_y').value

        gui_feedback_msg.isonborder = self.pedestrian.getonborder()

        gui_feedback_msg.vehicle_x = self.vehicle.getx()
        gui_feedback_msg.vehicle_y = self.vehicle.gety()
        gui_feedback_msg.vehicle_x_speed = self.vehicle.getxspeed()
        gui_feedback_msg.vehicle_y_speed = self.vehicle.getyspeed()
        gui_feedback_msg.vehicle_displaymessage = self.vehicle.getdisplaymessage()
        gui_feedback_msg.vehicle_x_init = self.get_parameter('veh_initial_pos_x').value

        gui_feedback_msg.is_simulation_reset = self.is_simulation_reset

        self.publisher_gui_feedback.publish(gui_feedback_msg)

        # Publish log data:
        if LOG == True:
            gui_log_msg = Log()
            gui_log_msg.t_stamp = toc
            gui_log_msg.pedestrian_x = self.pedestrian.getx()
            gui_log_msg.pedestrian_y = self.pedestrian.gety()
            gui_log_msg.vehicle_x = self.vehicle.getx()
            gui_log_msg.vehicle_y = self.vehicle.gety()
            gui_log_msg.distance = adist
            gui_log_msg.pedestrian_x_speed = self.pedestrian.getxspeed()
            gui_log_msg.pedestrian_y_speed = self.pedestrian.getyspeed()
            gui_log_msg.vehicle_x_speed = self.vehicle.getxspeed()
            gui_log_msg.vehicle_y_speed = self.vehicle.getyspeed()
            self.publisher_log.publish(gui_log_msg)

        if self.running == False:

            if LOG == True:
                # During shutdown process we basically tell the log node,
                # that there will be no more data published, and the log node
                # can save the data to a file.

                # Check whether the log node exists:
                stream = os.popen('ros2 node list')
                output = stream.read()
                if "/log" in output:
                    # Check whether service for saving the logfiles exists:
                    while not self.client_savelogfiles.wait_for_service(timeout_sec=1.0):
                        self.get_logger().info("Waiting for service 'savelogfiles' ... ")

                    # Create the request object:
                    req = Empty.Request()

                    # Service call with request object:
                    future = self.client_savelogfiles.call_async(req)
                    # (We dont care about this object, since if data is present in the
                    # directory, than the service was succesful)
                else:
                    self.get_logger().info("Could not save files, log node does not exists!")

            self.get_logger().info("Shutdown")
            pygame.quit()
            sys.exit()

        # --- End of timer_callback ---

    def draw_background_objects(self):

        # Draw road rectangle:
        pygame.draw.rect(self.display_surface, self.road_color, self.road_rect)

        # Draw vehicle information on the bottom of the screen:
        switcher = {
            0: "Vehicle information:",
            #1: "a_x = unknown", self.vehicle.
            1: "v_x = " + str( round(self.vehicle.getxspeed(),1) ) + " [m/s]",
            2: "x = " + str( round(self.vehicle.getx(),1) ) + " [m]",
            3: "y = " + str( round(self.vehicle.gety(),1) ) + " [m]",
            4: "Display message = " + str(self.vehicle.getdisplaymessage()),
            5: "Pedestrian information:",
            6: "v_x = " + str( round(self.pedestrian.getxspeed(), 1)) + " [m/s]",
            7: "v_y = " + str( round(self.pedestrian.getyspeed(), 1)) + " [m/s]",
            8: "x = " + str( round(self.pedestrian.getx(), 1)) + " [m]",
            9: "y = " + str( round(self.pedestrian.gety(), 1)) + " [m]",
            10: "Pedestrian intention = " + str( round(self.pedestrian.getintention(), 1)),
        }
        row_count = len(switcher)
        for i in range(row_count):
            textsurf = FONT.render(switcher.get(i,"---"), True, TEXTCOLOR, None)
            textposition = textsurf.get_rect()
            textheight = textposition.height
            textposition.topleft = (10, self.display_height - 10 - row_count*textheight + i*textheight)
            self.display_surface.blit(textsurf, textposition)




    def draw_distances(self):
        # Draw vertical distance:
        # pygame.draw.line(self.display_surface, BLACK, (self.meter2pixel(self.pedestrian.getx()), self.meter2pixel(self.pedestrian.gety())),
        #                     (self.meter2pixel(self.pedestrian.getx()),self.meter2pixel(self.vehicle.gety())), 3)
        # vdist = (self.pedestrian.gety() - self.vehicle.gety())
        # vdist = abs(vdist)
        # textsurf = FONT.render("{:.2f} [m]".format(vdist), True, TEXTCOLOR, WHITE)
        # textrect = textsurf.get_rect()
        # textrect.midleft = (self.meter2pixel(self.pedestrian.getx()),
        #                     self.meter2pixel(self.vehicle.gety() + (self.pedestrian.gety() - self.vehicle.gety())/2))
        # self.display_surface.blit(textsurf, textrect)

        # Draw horizontal distance:
        # pygame.draw.line(self.display_surface, BLACK, (self.meter2pixel(self.vehicle.getx()), self.meter2pixel(self.vehicle.gety())),
        #                     (self.meter2pixel(self.pedestrian.getx()),self.meter2pixel(self.vehicle.gety())), 3)
        # hdist = (self.vehicle.getx() - self.pedestrian.getx())
        # hdist = abs(hdist)
        # textsurf = FONT.render("{:.2f} [m]".format(hdist), True, TEXTCOLOR, WHITE)
        # textrect = textsurf.get_rect()
        # textrect.midbottom = (self.meter2pixel(self.vehicle.getx() + (self.pedestrian.getx() - self.vehicle.getx())/2),
        #                     self.meter2pixel(self.vehicle.gety()))
        # self.display_surface.blit(textsurf, textrect)

        # Draw absolute distance:
        pygame.draw.line(self.display_surface, BLACK, (self.meter2pixel(self.pedestrian.getx()), self.meter2pixel(self.pedestrian.gety())),
                           (self.meter2pixel(self.vehicle.getx()),self.meter2pixel(self.vehicle.gety())), 3)
        adist = math.sqrt((self.pedestrian.getx() - self.vehicle.getx())**2 +
                            (self.pedestrian.gety() - self.vehicle.gety())**2)
        textsurf = FONT.render("{:.2f} [m]".format(adist), True, TEXTCOLOR, WHITE)
        textrect = textsurf.get_rect()
        textrect.midtop = ((self.meter2pixel(self.pedestrian.getx()) + self.meter2pixel(self.vehicle.getx()))/2,
                           (self.meter2pixel(self.pedestrian.gety()) + self.meter2pixel(self.vehicle.gety()))/2)
        self.display_surface.blit(textsurf, textrect)

        return adist


def main(args=None):
    rclpy.init(args=args)

    simulation_gui_node = SimulationGuiNode()

    rclpy.spin(simulation_gui_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    simulation_gui_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
