#!/usr/bin/env python3

from ast import Param
import os
from glob import glob

from numpy import size

import time

class MeasureTime:
    """Class to measure elapsed time with some extras.
    """
    def __init__(self, initial_elapsed_time):
        self.initial_elapsed_time = initial_elapsed_time
        self.firstrun = False
        self.first_run_time = 0.0
        self.penultimate_run_time = 0.0
        self.current_run_time = 0.0
        self.commulative_time_since_first_run = 0.0

    def dt_since_last_run(self):
        """On first run, it returns a default value, during further runs,
           it returns the elapsed time in seconds since the latest run.
        """
        if self.firstrun == False:
            self.first_run_time = time.time()
            self.current_run_time = time.time()
            self.firstrun = True
            return self.initial_elapsed_time
        else:
            self.penultimate_run_time = self.current_run_time
            self.current_run_time = time.time()
            return self.current_run_time - self.penultimate_run_time

    def dt_since_first_run(self):
        """Commulative time since first run.
        """
        if self.firstrun == False:
            self.first_run_time = time.time()
            self.current_run_time = time.time()
            self.firstrun = True
            return self.initial_elapsed_time
        else:
            self.current_run_time = time.time()
            self.commulative_time_since_first_run = self.initial_elapsed_time + self.current_run_time - self.first_run_time
            return self.commulative_time_since_first_run

    def reset(self):
        """Reset time values.
        """
        self.firstrun = False
        self.first_run_time = 0.0
        self.penultimate_run_time = 0.0
        self.current_run_time = 0.0
        self.commulative_time_since_first_run = 0.0




test = MeasureTime(0.5)

print(test.dt_since_first_run())

time.sleep(0.2)
print(test.dt_since_first_run())
test.reset()

print(test.dt_since_first_run())
time.sleep(0.4)
print(test.dt_since_first_run())
time.sleep(0.2)
print(test.dt_since_first_run())
