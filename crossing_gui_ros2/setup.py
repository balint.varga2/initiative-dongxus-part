from setuptools import setup
from glob import glob
import os


package_name = 'crossing_gui_ros2'
simulation_node_functions = 'crossing_gui_ros2/simulation_node_functions'
#images = 'crossing_gui_ros2/img' # doesnt do anything :(
# i am trying to copy the contents of the img folder to the install folder
# in order to not to use global absolute pathes for the images like right now
# it only copies python packages

setup(
    name=package_name,
    version='0.0.0',
    # The next line copies the python packages to the install dir, e.g. the decision_functions:
    packages=[package_name, simulation_node_functions],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),

        # Install the launch files:
        (os.path.join('share', package_name), glob('launch/*_launch.py')),

        # Install the YAML config files:
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml')),

        # Additional resources:
        (os.path.join('lib', 'python3.8', 'site-packages', package_name, 'imgs'), glob('crossing_gui_ros2/imgs/*.png') ),
        (os.path.join('lib', 'python3.8', 'site-packages', package_name, 'fonts'), glob('crossing_gui_ros2/fonts/*.ttf') ),
        #

        # Syntax:  ( target_directory, [list of files to be put there] )
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='dongxu',
    maintainer_email='ubtef@student.kit.edu',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'control_vehicle = crossing_gui_ros2.control_vehicle:main',
            'control_pedestrian = crossing_gui_ros2.control_pedestrian:main',
            'pedestrian_crossing_simulation = crossing_gui_ros2.pedestrian_crossing_simulation:main',
            'log = crossing_gui_ros2.log:main',
            'control_vehicle_decision_function = crossing_gui_ros2.decision_function',
            'ped_motion_vanilla = crossing_gui_ros2.ped_motion_vanilla:main',
            'ped_motion_2D = crossing_gui_ros2.ped_motion_2D:main',
            'ped_motion_1D = crossing_gui_ros2.ped_motion_1D:main'

        # For example:
        # test = my_python_pkg.my_python_node:main
        # 'test' is the name of the executable after the script is installed
        # 'my_python_pkg.my_python_node:main' means execute the main() function
        # inside of the 'my_python_node.py' file, the node starts here.
        ],
    },
)
