# Cooperative control for human-vehicle interaction

## Name
Comparison of different methods in mixed traffic scenarios

## Description
This project is about comparing Model Predictive Control(MPC) and Reinforcement Learning method(PPO) in such kind of scenario where vehicle interacts with human.

## Visuals
![Example](./documentations/example.jpeg)

## Installation

if You want to train PPO model, please locate to corresponding directory and install the environment package:
```
cd /Path/To/Environment/Directory
pip install .
```

if You want to use utils locally, please locate to corresponding directory and install package:
```
cd /Path/To/Utils
pip install .
```

## Usage
test with automated human motion
```
ros2 launch crossing_gui_ros2 crossing_gui_ros2_automated_launch.py
ros2 launch dynamic_negotiation ped_veh_interaction_launch.py
```

test with keyboard  
```
ros2 launch crossing_gui_ros2 crossing_gui_ros2_keyboard_launch.py
ros2 launch dynamic_negotiation ped_veh_interaction_keyboard_launch.py
```

You could set parameters(e.g. controller) when running launch file or node
```
ros2 launch dynamic_negotiation ped_veh_interaction_launch.py controller:=<linear_mpc> <non_linear_mpc_explicit> <non_linear_mpc_implicit> <ppo> <rule_based>

ros2 run dynamic_negotiation dynamic_negotiation --ros-args -p controller:=<controller>
```
or set it using 
`ros2 param set /dynamic_negotiation controller <controller>`

Now you could also set human model, crossing_mode and intention when launching automatically controlled pedestrian motion model
```
human_model:=<SFM/MDP>
cross:=<True/False>
ped_intention:=<0.0-1.0>
```

Gym Environment is in folder `reinforcement_learning`

PPO model parameter is in `/dongxu-master-thesis/reinforcement_learning/PPO/sb3/ppo_v1_local`
PPO training file is in `/dongxu-master-thesis/reinforcement_learning/PPO/sb3/train.py`

DQN model parameter is in `/dongxu-master-thesis/reinforcement_learning/DQN/sb3/dqn_v1`
DQN training file is in `/dongxu-master-thesis/reinforcement_learning/DQN/sb3/train.py`

## Dependencies
```
casadi
sb3-contrib
imageio
wandb
tensorboard
scipy==1.10.0
numpy==1.23.5
stable-baselines3==1.8.0
stable-baselines3[extra]
gym==1.24.1

Intalling some packages may cover other packages, please check final version
```

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
