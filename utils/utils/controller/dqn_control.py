import torch
import numpy as np
import os
from stable_baselines3 import DQN
from utils.controller.vanilla_mpc import vanilla_mpc
import gym
import human_vehicle_interaction_env
from human_vehicle_interaction_env.wrappers import *

is_with_intention = True


class DQNAgent:
    def __init__(self):
        env = gym.make('hvi-2d')
        if not is_with_intention:
            env = WithoutIntention(env)
        self.env = DiscreteAction(env)

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model_weights_path = os.getenv("HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/DQN/sb3/dqn_v1'
        self.agent = DQN.load(self.model_weights_path, env=self.env,
                              custom_objects={'observation_space': self.env.observation_space, 'action_space': self.env.action_space})
        self.discrete_acc = [-4.0, -2.0, 0.0, 2.0, 4.0]

        self.safe_distance_y = 1.5

    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > self.safe_distance_y
        is_veh_passed = veh.x_var > ped.x_var
        is_ped_passed = False
        if (ped.y_var - veh.y_var) * ped.yspeed_ms_var > 0 and is_ped_outside_road:
            is_ped_passed = True

        # if pedestrian or vehicle has passed
        if is_veh_passed or is_ped_passed:
            return vanilla_mpc(ped, veh)

        if is_with_intention:
            states = np.array([ped.x_var - veh.x_var,
                               ped.y_var - veh.y_var,
                               ped.xspeed_ms_var,
                               ped.yspeed_ms_var,
                               veh.xspeed_ms_var,
                               ped.intention_var])
        else:
            states = np.array([ped.x_var - veh.x_var,
                               ped.y_var - veh.y_var,
                               ped.xspeed_ms_var,
                               ped.yspeed_ms_var,
                               veh.xspeed_ms_var])
        acc_index = self.agent.predict(states, deterministic=True)[0]
        veh_acc_cmd_ms2 = self.discrete_acc[acc_index]
        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

        return veh_acc_cmd_ms2, pub_text



