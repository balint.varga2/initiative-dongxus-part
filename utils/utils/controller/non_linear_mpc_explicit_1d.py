import casadi as ca
import numpy as np
import copy

from utils.human_motion.pedestrian_sigmoid import ped_sigmoid_model
from utils.controller.vanilla_mpc import vanilla_mpc
from utils.controller.mpc_with_constant_human_speed import MPCwithConstantHumanSpeed


class NonLinearMPC_explicit:
    def __init__(self, time_step=0.55, w1=7.93, w2=4.66, w3=300.0):
        self.time_step = time_step
        self.weight1 = w1
        self.weight2 = w2
        self.weight3 = w3

        self.safe_distance = 3.0
        self.safe_distance_y = 1.5
        self.pred_horizon_para = 6
        self.ctrl_horizon_para = 6
        self.min_veh_acc_para = - 5.0
        self.max_veh_acc_para = 5.0

    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > self.safe_distance_y
        w1 = self.weight1
        w2 = self.weight2
        w3 = self.weight3 * ped.intention_var if is_ped_outside_road else self.weight3

        self.safe_distance = self.safe_distance * ped.intention_var if is_ped_outside_road else self.safe_distance

        is_veh_passed = veh.x_var > ped.x_var
        is_ped_passed = False
        if (ped.y_var - veh.y_var) * ped.yspeed_ms_var > 0 and is_ped_outside_road:
            is_ped_passed = True

        # if pedestrian or vehicle has passed
        if is_ped_passed or is_veh_passed:
            return vanilla_mpc(ped, veh)

        # change reference speed of human for calculation
        if ped.y_var > veh.y_var: ped.yspeed_ref_para = - ped.yspeed_ref_para

        # set states and inputs for MPC
        # states
        veh_x = ca.SX.sym('veh_x')
        veh_x_speed = ca.SX.sym('veh_x_speed')
        ped_y = ca.SX.sym('ped_y')
        ped_y_speed = ca.SX.sym('ped_y_speed')
        states = ca.vertcat(veh_x, veh_x_speed, ped_y, ped_y_speed)
        n_states = states.size()[0]

        # inputs
        veh_acc_x = ca.SX.sym('veh_acc_x')
        inputs = ca.vertcat(veh_acc_x)
        n_inputs = inputs.size()[0]

        states = ca.SX.sym('states', n_states, self.pred_horizon_para + 1)
        inputs = ca.SX.sym('inputs', n_inputs, self.ctrl_horizon_para)
        model_params = ca.SX.sym('model_params', 4)  # Parameter: [veh_x, veh_x_speed, ped_y, ped_y_speed]

        states[:, 0] = model_params[:]

        ped_ = copy.deepcopy(ped)
        veh_ = copy.deepcopy(veh)

        # set forward kinematic model
        for i in range(self.pred_horizon_para):
            if i in range(self.ctrl_horizon_para):
                states[0, i + 1] = states[0, i] + self.time_step * states[1, i] + 0.5 * inputs[0, i] * self.time_step ** 2
                states[1, i + 1] = states[1, i] + self.time_step * inputs[0, i]

                veh_.x_var = states[0, i]
                veh_.xspeed_ms_var = states[1, i]
                ped_.y_var = states[2, i]
                ped_.yspeed_ms_var = states[3, i]
                states[2, i + 1] = states[2, i] + self.time_step * states[3, i]
                states[3, i + 1] = ped_sigmoid_model(ped_, veh_)
            else:
                states[0, i + 1] = states[0, i] + self.time_step * states[1, self.ctrl_horizon_para]
                states[1, i + 1] = states[1, i]

                states[2, i + 1] = states[2, i] + self.time_step * states[3, i]
                states[3, i + 1] = ped_sigmoid_model(ped_, veh_)

        # set cost function
        obj = 0
        for i in range(self.ctrl_horizon_para):
            obj += w1 * inputs[0, i] ** 2
            obj += w2 * (veh.xspeed_ref_para - states[1, i]) ** 2

            pred_ped_veh_distance = np.sqrt((ped.x_var - states[0, i + 1]) ** 2 + (veh.y_var - states[2, i + 1]) ** 2)
            obj += w3 * 1 / (pred_ped_veh_distance ** 4 + 1e-4) * (0.8 ** i)

        # define constraints
        g = []
        for i in range(1, self.pred_horizon_para + 1):
            g.append(states[1, i])

            pred_ped_veh_distance = np.sqrt((ped.x_var - states[0, i]) ** 2 + (veh.y_var - states[2, i]) ** 2)
            g.append(pred_ped_veh_distance)

        # define non-linear programming solver
        nlp = {'f': obj, 'x': ca.reshape(inputs, -1, 1), 'p': model_params, 'g': ca.vertcat(*g)}

        opts_setting = {'ipopt.max_iter': 500, 'ipopt.print_level': 0, 'print_time': 0, 'ipopt.acceptable_tol': 1e-8, \
                        'ipopt.acceptable_obj_change_tol': 1e-6}
        nlp_solver = ca.nlpsol('solver', 'ipopt', nlp, opts_setting)

        # set lower bound and upper bound for inputs and constraints
        lbg = []
        ubg = []
        lbx = []
        ubx = []

        for i in range(self.pred_horizon_para):
            lbg.append(0.0)
            ubg.append(veh.xspeed_max_para)

            lbg.append(self.safe_distance)
            ubg.append(ca.inf)

            if i in range(self.ctrl_horizon_para):
                lbx.append(self.min_veh_acc_para)
                ubx.append(self.max_veh_acc_para)

        # set model parameters
        nlp_params = [veh.x_var, veh.xspeed_ms_var, ped.y_var, ped.yspeed_ms_var]

        # call solver
        try:
            res = nlp_solver(x0=[0.0] * self.ctrl_horizon_para, p=nlp_params, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)
            veh_acc_cmd_ms2 = float(res['x'][0, 0])
        except Exception or RuntimeError:
            veh_acc_cmd_ms2 = self.min_veh_acc_para

        acc_ = MPCwithConstantHumanSpeed().eval(ped, veh)[0]
        veh_acc_cmd_ms2 = min(veh_acc_cmd_ms2, acc_)

        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"
        # change back, otherwise it will influence next calculation
        if ped.y_var > veh.y_var: ped.yspeed_ref_para = - ped.yspeed_ref_para

        if veh.xspeed_ms_var + veh_acc_cmd_ms2 * 0.1 < 0.0:
            veh_acc_cmd_ms2 = 0.0

        return veh_acc_cmd_ms2, pub_text
