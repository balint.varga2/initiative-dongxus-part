
class HumanLeadController:
    def __init__(self):

        self.x_distance_to_ped = 20
        self.ped_distance_from_road_threshold_para = 5
        
        # Parameters for calculating the acceleration
        self.veh_dec_gain_para = 9.5
        self.veh_max_dec_para = 5.0  # maximal deceleration
        self.veh_max_speed_para = 7.0
        self.veh_normal_acc = 1.5
        
        # If the vehicle is out of safe stop distance, it could keep a safe speed and doesn't need to decelerate too much
        self.veh_safe_stop_distance_para = 6
        self.veh_safe_speed_para = 1.2

        
    def veh_crossing(self, ped, veh):
        acc_start = (veh.xspeed_ref_para-veh.xspeed_ms_var) * 1 
        
        return acc_start if acc_start < self.veh_normal_acc else self.veh_normal_acc
    
        
    

    def veh_stopping(self, ped, veh):
        is_ped_far_from_veh = self.ped_veh_distance_var >= self.veh_safe_stop_distance_para

        if is_ped_far_from_veh and veh.xspeed_ms_var <= self.veh_safe_speed_para:
            return 0.0
        else:
            return max(- self.veh_max_dec_para,- self.veh_dec_gain_para * veh.xspeed_ms_var / (1 + self.ped_veh_distance_var))




    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > veh.road_width # half-WIDTH of the road, TODO: WIVW checking 

        is_veh_passed = veh.x_var > ped.x_var
        
        is_ped_passed = False
        if (ped.y_var - veh.y_var) < 0.5 and is_ped_outside_road:
            is_ped_passed = True
        
        self.ped_veh_distance_var = ped.x_var - veh.x_var
        self.ped_distance_from_road_var = ped.y_var - veh.y_var

        is_pedestrian_close_to_road = abs(self.ped_distance_from_road_var) <= self.ped_distance_from_road_threshold_para
        
        
        
        veh_acc_cmd_ms2 = 0.0
        if is_veh_passed or is_ped_passed:
            veh_acc_cmd_ms2 = self.veh_crossing(ped, veh)
            print(1)
        elif is_pedestrian_close_to_road and self.x_distance_to_ped > abs(self.ped_veh_distance_var):
            veh_acc_cmd_ms2 = self.veh_stopping(ped, veh)
            print(2)
        else:
            veh_acc_cmd_ms2 = self.veh_crossing(ped, veh)
            print(3)

        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

        return veh_acc_cmd_ms2, pub_text
