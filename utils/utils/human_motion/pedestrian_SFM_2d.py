import copy
import numpy as np
from utils.common.add_noise import add_noise


def social_force_model_2d(pedestrian, vehicle, cross=True):
    # The pedestrian goes away from street
    if (pedestrian.y_var - vehicle.y_var) * pedestrian.yspeed_ms_var > 0:
        return [0.0, pedestrian.yspeed_ref_para] if pedestrian.y_var > vehicle.y_var else [0.0, - pedestrian.yspeed_ref_para]

    dt = 0.1
    sigma = 0.2
    waiting_area = 2.0
    k_des = 200.0 # 300.0
    m_p = 70.0
    A_veh = 200.0
    b_veh = 1.0
    TTC = (pedestrian.x_var - vehicle.x_var) / (vehicle.xspeed_ms_var + 0.001)
    t_gap = 2.5 + np.random.normal(0.0, 0.1)

    if cross == True:
        target_pos = np.array([0.0, -2.0])
    else:
        target_pos = np.array([0.0, 2.0])

    veh_pos = np.array([vehicle.x_var - pedestrian.x_var, 0.0])
    ped_pos = np.array([0.0, pedestrian.y_var - vehicle.y_var])
    ped_speed = np.array([pedestrian.xspeed_ms_var, pedestrian.yspeed_ms_var])

    # set ped_y as positive, set ped_speed_y as negative
    if pedestrian.y_var < vehicle.y_var:
        ped_pos = - ped_pos
        ped_speed[1] = - ped_speed[1]

    # The pedestrian doesn't want to cross
    if not cross:
        ped_yspeed = min(pedestrian.yspeed_ref_para, 0.5 * abs(target_pos[1] - ped_pos[1]))
        if pedestrian.y_var > vehicle.y_var:
            if ped_pos[1] > target_pos[1]:
                return [pedestrian.xspeed_ms_var, -ped_yspeed]
            else:
                return [pedestrian.xspeed_ms_var, ped_yspeed]
        else:
            if ped_pos[1] > target_pos[1]:
                return [pedestrian.xspeed_ms_var, ped_yspeed]
            else:
                return [pedestrian.xspeed_ms_var, -ped_yspeed]

    vec_v2p = ped_pos - veh_pos
    distance_v2p = np.linalg.norm(vec_v2p) - 2.0
    n_v2p = vec_v2p / distance_v2p

    ped_speed_optimal = pedestrian.yspeed_ref_para
    is_vehicle_go_through = vehicle.x_var > pedestrian.x_var
    is_ped_in_waiting_area = abs(pedestrian.y_var - vehicle.y_var) < waiting_area
    is_ped_crossing = abs(pedestrian.y_var - vehicle.y_var) < 1.5

    ped_speed_desired = ped_speed_optimal * (target_pos - ped_pos) / np.sqrt(np.linalg.norm(target_pos - ped_pos) ** 2 + sigma ** 2)
    f_des = k_des * (ped_speed_desired - ped_speed)

    if is_vehicle_go_through or target_pos[1] >= 0.0:
        f_total = f_des
    else:
        if not is_ped_in_waiting_area:
            f_total = f_des
        else:
            f_veh = A_veh * np.exp(-b_veh * distance_v2p) * n_v2p

            if is_ped_crossing:
                d_rem = ped_pos[1] - target_pos[1]
                TTF = d_rem / ped_speed_optimal
                if TTC < TTF:
                    ped_speed_optimal = max(pedestrian.yspeed_max_para, d_rem / TTC)
                    ped_speed_desired = ped_speed_optimal * (target_pos - ped_pos) / np.sqrt(
                        np.linalg.norm(target_pos - ped_pos) ** 2 + sigma ** 2)
                    f_des = k_des * (ped_speed_desired - ped_speed)
                f_total = f_des + f_veh
            else:
                if TTC > t_gap:
                    f_total = f_des + f_veh
                else:
                    return [0.0, 0.0]

    a_ped = f_total / m_p
    ped_speed_next = ped_speed + a_ped * dt

    # if pedestrian wants to cross, he cannot step back
    if cross and ped_speed_next[1] >= 0:
        ped_speed_next[1] = 0

    # unpack
    if pedestrian.y_var < vehicle.y_var: ped_speed_next[1] = - ped_speed_next[1]

    # avoid suddenly large change in speed
    ped_speed_next[0] = np.clip(ped_speed_next[0], -1.0, 1.0)
    ped_speed_next[1] = np.clip(ped_speed_next[1], -1.4, 1.4)

    return ped_speed_next


def predict_ped_with_SFM_2d(pedestrian, vehicle, time_step, pred_horizon, cross=True):
    # add noise
    pedestrian = add_noise(pedestrian)
    vehicle = add_noise(vehicle)

    ped_pred_pos = []
    new_pedestrian = copy.deepcopy(pedestrian)
    new_vehicle = copy.deepcopy(vehicle)
    for i in range(pred_horizon):

        next_ped_speed = social_force_model_2d(new_pedestrian, new_vehicle, cross=cross)
        new_pedestrian.x_var += next_ped_speed[0] * time_step
        new_pedestrian.y_var += next_ped_speed[1] * time_step
        new_pedestrian.xspeed_ms_var = next_ped_speed[0]
        new_pedestrian.yspeed_ms_var = next_ped_speed[1]
        new_vehicle.x_var += new_vehicle.xspeed_ms_var * time_step

        ped_pred_pos.append((new_pedestrian.x_var, new_pedestrian.y_var))

    return ped_pred_pos



