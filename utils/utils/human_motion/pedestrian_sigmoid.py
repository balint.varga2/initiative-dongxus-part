import numpy as np


def ped_sigmoid_model(pedestrian, vehicle):

    t1 = (pedestrian.x_var - vehicle.x_var) / np.sqrt(vehicle.xspeed_ms_var ** 2 + 0.1)
    t2 = (vehicle.y_var - pedestrian.y_var) / (pedestrian.yspeed_ref_para + 0.001)
    TTC = t1 - t2 - 1.0
    ped_new_speed = 1 / (1 + np.exp(-TTC)) * pedestrian.yspeed_ref_para

    return ped_new_speed



