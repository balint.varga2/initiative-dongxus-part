from setuptools import setup
from glob import glob
import os

package_name = 'utils'
common = 'utils/common'
controller = 'utils/controller'
human_motion = 'utils/human_motion'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, common, controller, human_motion],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Install the launch files:
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='yang',
    maintainer_email='ubtef@student.kit.edu',
    description='Necessary Tools for Dynamic Negotiation',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
