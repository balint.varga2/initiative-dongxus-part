import numpy as np
import pyswarms as ps
import yaml
from sim_model_objects import Object
from social_force_model import social_force_model

NUM_PARAMETERS = 13
DEBUG_MODE = 0

class ParticleSwarmOptimization():

    def __init__(self):

        # parameters
        self.low_ped_speed_threshold_discounting = 0.5
        self.ped_low_yspeed = 0.8
        self.ped_high_yspeed = 1.6
        self.ped_low_intention = 0.3
        self.ped_high_intention = 0.6
        self.ped_distance_from_road_threshold = 3.0
        self.veh_dec_gain = 6.0
        self.veh_max_dec = 5.0
        self.veh_max_speed = 8.0
        self.veh_safe_stop_distance = 5.0
        self.veh_safe_speed = 2.0
        self.veh_high_speed = 6.0
        self.veh_normal_acc = 0.5


        # state of pedestrian and vehicle
        self.veh_init_xspeed = [7.0, 7.0, 7.0]
        self.ped_init_x = [15.0, 13.0, 10.0]
        self.ped_init_y = [4.0, 3.5, 3.0]

        self.TIME_PERIOD = 0.1

        self.veh = Object()

        self.ped = Object()
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.5
        self.ped.intention_var = 1.0  # between 0 and 1

        self.ped_distance_from_road = self.ped.y_var - self.veh.y_var
        self.ped_veh_distance = self.ped.x_var - self.veh.x_var
        self.discounted_ped_intention = 0.0
        self.count = 0
        self.collision_area = 1.5

        self.veh_acc_cmd = 0.0

    def reset_model_state(self, case_i):
        self.veh.x_var = 0.0
        self.veh.xspeed_ms_var = self.veh_init_xspeed[case_i]

        self.ped.x_var = self.ped_init_x[case_i]
        self.ped.y_var = self.ped_init_y[case_i]
        self.ped.y_init_var = self.ped_init_y[case_i]
        self.ped.yspeed_ms_var = - self.ped.yspeed_ref_para


        self.ped_distance_from_road = self.ped.y_var - self.veh.y_var
        self.ped_veh_distance = self.ped.x_var - self.veh.x_var

        self.discounted_ped_intention = 0.0
        self.count = 0
        self.veh_acc_cmd = 0.0

    def intention_discounting(self):
        is_start_discounting = abs(self.ped_distance_from_road) > self.ped_distance_from_road_threshold
        if is_start_discounting and abs(self.ped.yspeed_ms_var) < self.low_ped_speed_threshold_discounting:
            self.count += 1
        else:
            self.count = 0
        self.discounted_ped_intention = self.ped.intention_var * pow(0.9, self.count * 0.5)

    def veh_crossing(self):
        self.veh_acc_cmd = self.veh_normal_acc if self.veh.xspeed_ms_var < self.veh_max_speed else 0.0

    def veh_stopping(self):
        is_ped_far_from_veh = self.ped_veh_distance >= self.veh_safe_stop_distance
        #veh_min_dec = 0.5 * self.veh_xspeed_ms_var ** 2 / self.ped_veh_distance_var

        if is_ped_far_from_veh and self.veh.xspeed_ms_var <= self.veh_safe_speed:
            self.veh_acc_cmd = 0.0
        else:
            self.veh_acc_cmd = min(0.0, max(- self.veh_max_dec, - self.veh_dec_gain * self.veh.xspeed_ms_var / (1 + self.ped_veh_distance)))
            # self.veh_acc_cmd_ms2 = max(- self.veh_max_dec_para, min(- veh_min_dec, - self.veh_dec_gain_para * self.veh_xspeed_ms_var / (1 + self.ped_veh_distance_var)))

    def can_veh_safe_cross(self):
        is_pedestrian_close_to_road = abs(self.ped_distance_from_road) <= self.ped_distance_from_road_threshold
        if self.ped_veh_distance < self.ped_distance_from_road and self.veh.xspeed_ms_var >= self.veh_high_speed \
                and not is_pedestrian_close_to_road:
            return True
        return False

    def dynamic_negotiation(self, params):

        self.low_ped_speed_threshold_discounting = params[0]
        self.ped_low_yspeed = params[1]
        self.ped_high_yspeed = params[2]
        self.ped_low_intention = params[3]
        self.ped_high_intention = params[4]
        self.ped_distance_from_road_threshold = params[5]
        self.veh_dec_gain = params[6]
        self.veh_max_dec = params[7]
        self.veh_max_speed = params[8]
        self.veh_safe_stop_distance = params[9]
        self.veh_safe_speed = params[10]
        self.veh_high_speed = params[11]
        self.veh_normal_acc = params[12]

        if self.ped.y_init_var > 0:
            is_pedestrian_crossed = self.ped_distance_from_road + self.collision_area <= 0
        else:
            is_pedestrian_crossed = self.ped_distance_from_road - self.collision_area >= 0
        is_pedestrian_close_to_road = abs(self.ped_distance_from_road) <= self.ped_distance_from_road_threshold
        is_vehicle_gone_through = self.ped_veh_distance < 0.0
        is_pedestrian_go_away = self.ped_distance_from_road * self.ped.yspeed_ms_var > 0

        if is_vehicle_gone_through or is_pedestrian_crossed:
            self.veh_crossing()
        elif not self.can_veh_safe_cross():
            if abs(self.ped_distance_from_road) < self.collision_area:
                self.veh_stopping()
            elif is_pedestrian_go_away:
                self.veh_crossing()
            elif is_pedestrian_close_to_road and abs(self.ped.yspeed_ms_var) > 0.0:
                self.veh_stopping()
            elif abs(self.ped.yspeed_ms_var) > self.ped_high_yspeed or \
                    self.discounted_ped_intention > self.ped_high_intention:
                self.veh_stopping()
            elif self.ped_low_yspeed <= abs(self.ped.yspeed_ms_var) <= self.ped_high_yspeed and \
                    self.ped_low_intention <= self.discounted_ped_intention <= self.ped_high_intention:
                self.veh_stopping()
            else:
                self.veh_crossing()
        else:
            self.veh_crossing()

    def eval_func(self, params):
        res = 0.0
        k1 = 1
        k2 = 1
        k3 = 5
        k4 = 0

        n = 1 if DEBUG_MODE else len(self.ped_init_x)
        for i in range(n):
            t = 0
            max_veh_acc = 0.0
            min_ped_veh_distance = float('inf')
            ttc = 0

            # reset model state
            self.reset_model_state(i)

            # simulation of motion
            while self.veh.x_var <= self.ped.x_var:
                # debug
                if DEBUG_MODE:
                    print(f'time: {t}')
                    print(f'current ped_x: {self.ped.x_var}')
                    print(f'current ped_y: {self.ped.y_var}')
                    print(f'current ped_yspeed: {self.ped.yspeed_ms_var}')
                    print(f'current veh_x: {self.veh.x_var}')
                    print(f'current veh_xspeed: {self.veh.xspeed_ms_var}')
                    print(f'current veh_acc_cmd: {self.veh_acc_cmd}')
                    print('--------------------')

                # calculate command
                self.intention_discounting()
                self.dynamic_negotiation(params)
                self.ped.yspeed_ms_var = social_force_model(self.ped, self.veh)

                # update state
                self.veh.x_var += self.veh.xspeed_ms_var * self.TIME_PERIOD + 0.5 * self.veh_acc_cmd * self.TIME_PERIOD ** 2
                self.veh.xspeed_ms_var += self.TIME_PERIOD * self.veh_acc_cmd
                self.ped.y_var += self.TIME_PERIOD * self.ped.yspeed_ms_var

                self.ped_veh_distance = self.ped.x_var - self.veh.x_var
                self.ped_distance_from_road = self.ped.y_var - self.veh.y_var

                # ttc
                if self.ped_distance_from_road >= 0 and self.ped_distance_from_road + self.TIME_PERIOD * self.ped.yspeed_ms_var <= 0:
                    ttc = self.ped_veh_distance / (self.veh.xspeed_ms_var + 0.01)
                # t
                t += self.TIME_PERIOD
                # min_ped_veh_distance
                if self.ped_distance_from_road >= 0:
                    min_ped_veh_distance = min(min_ped_veh_distance, np.sqrt(self.ped_veh_distance ** 2 + self.ped_distance_from_road ** 2))
                # max_veh_acc
                max_veh_acc = max(max_veh_acc, abs(self.veh_acc_cmd))

            res += k1 * t + k2 * max_veh_acc ** 2 - k3 * min_ped_veh_distance if ttc == 0 else k1 * t + k2 * max_veh_acc - k3 * min_ped_veh_distance + k4 * 1 / ttc
        return res


def opt_func(particles):
    n_particales = particles.shape[0]
    res = []
    for i in range(n_particales):
        obj = ParticleSwarmOptimization()
        res.append(obj.eval_func(particles[i]))
    return np.array(res)

# debug
if DEBUG_MODE:
    params = [0.5, 0.6, 1.4, 0.2, 0.5, 4.0, 8.0, 5.0, 7.0, 6.0, 2.0, 7.0, 0.4] # parameter of defensive driver
    obj = ParticleSwarmOptimization()
    result = obj.eval_func(params)
    print(f'result of debug:{result}')
else:
    # Set-up hyperparameters
    options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}
    constraints = (np.array([0.4, 0.0, 1.0, 0.0, 0.5, 1.5, 2.0, 1.0, 5.0, 3.0, 1.0, 5.0, 2.0]),
                   np.array([0.6, 1.0, 2.0, 0.5, 1.0, 4.0, 10.0, 5.0, 10.0, 6.0, 3.0, 10.0, 4.0])
                   )
    # Call instance of PSO
    optimizer = ps.single.GlobalBestPSO(n_particles=130, dimensions=NUM_PARAMETERS, options=options, bounds=constraints)
    # Perform optimization
    cost, pos = optimizer.optimize(opt_func, iters=100)

    print(f'low_ped_speed_threshold_discounting: {pos[0]}')
    print(f'ped_low_yspeed: {pos[1]}')
    print(f'ped_high_yspeed: {pos[2]}')
    print(f'ped_low_intention: {pos[3]}')
    print(f'ped_high_intention: {pos[4]}')
    print(f'ped_distance_from_road_threshold: {pos[5]}')
    print(f'veh_dec_gain: {pos[6]}')
    print(f'veh_max_dec: {pos[7]}')
    print(f'veh_max_speed: {pos[8]}')
    print(f'veh_safe_stop_distance: {pos[9]}')
    print(f'veh_safe_speed: {pos[10]}')
    print(f'veh_high_speed: {pos[11]}')
    print(f'veh_normal_acc: {pos[12]}')

    # save paramters to yaml file
    optimal_driver_config_yaml = f"""
    vanilla_dynamic_negotiation:
      ros__parameters:
        low_ped_speed_threshold_discounting: {pos[0]}
        ped_low_yspeed: {pos[1]}
        ped_high_yspeed: {pos[2]}
        ped_low_intention: {pos[3]}
        ped_high_intention: {pos[4]}
        ped_distance_from_road_threshold: {pos[5]}
        veh_dec_gain: {pos[6]}
        veh_max_dec: {pos[7]}
        veh_max_speed: {pos[8]}
        veh_safe_stop_distance: {pos[9]}
        veh_safe_speed: {pos[10]}
        veh_high_speed: {pos[11]}
        veh_normal_acc: {pos[12]}
    """
    optimal_driver_config = yaml.safe_load(optimal_driver_config_yaml)
    with open('SFM_driver_config.yaml', 'w') as file:
        yaml.dump(optimal_driver_config, file)