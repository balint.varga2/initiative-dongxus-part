import numpy as np
import pyswarms as ps
import yaml
import time
import copy
import casadi as ca
from sim_model_objects import Object
from ped_sigmoid_model import ped_sigmoid_model
from social_force_model import social_force_model

NUM_PARAMETERS = 4
DEBUG_MODE = 0


class ParticleSwarmOptimization():

    def __init__(self):

        # parameters
        self.w1 = 1.0
        self.w2 = 1.0
        self.w3 = 1.0
        self.pred_horizon_para = 6
        self.ctrl_horizon_para = 6
        self.pred_time_step_para = 0.5
        self.min_veh_x_acc_para = - 5.0
        self.max_veh_x_acc_para = 5.0


        # state of pedestrian and vehicle
        self.veh_init_xspeed = [7.0, 7.0, 7.0]
        self.ped_init_x = [15.0, 13.0, 10.0]
        self.ped_init_y = [4.0, 3.5, 3.0]

        self.veh = Object()
        self.veh.xspeed_ref_para = 7.0
        self.veh.xspeed_max_para = 10.0

        self.ped = Object()
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.5
        self.ped.intention_var = 1.0  # between 0 and 1

        self.ped_distance_from_road = self.ped.y_var - self.veh.y_var
        self.ped_veh_distance = self.ped.x_var - self.veh.x_var
        self.collision_area = 1.5

        self.veh_acc_cmd = 0.0

    def reset_model_state(self, case_i):

        self.veh.x_var = 0.0
        self.veh.xspeed_ms_var = self.veh_init_xspeed[case_i]

        self.ped.x_var = self.ped_init_x[case_i]
        self.ped.y_var = self.ped_init_y[case_i]
        self.ped.y_init_var = self.ped_init_y[case_i]
        self.ped.yspeed_ms_var = - self.ped.yspeed_ref_para

        self.ped_distance_from_road = self.ped.y_var - self.veh.y_var
        self.ped_veh_distance = self.ped.x_var - self.veh.x_var

        self.veh_acc_cmd = 0.0

    def model_predictive_control(self, params):

        self.pred_time_step_para = params[0]
        self.w1 = params[1]
        self.w2 = params[2]
        self.w3 = params[3]

        # set states and inputs for MPC
        # states
        veh_x = ca.SX.sym('veh_x')
        veh_x_speed = ca.SX.sym('veh_x_speed')
        ped_y = ca.SX.sym('ped_y')
        ped_y_speed = ca.SX.sym('ped_y_speed')
        states = ca.vertcat(veh_x, veh_x_speed, ped_y, ped_y_speed)
        n_states = states.size()[0]

        # inputs
        veh_acc_x = ca.SX.sym('veh_acc_x')
        inputs = ca.vertcat(veh_acc_x)
        n_inputs = inputs.size()[0]

        states = ca.SX.sym('states', n_states, self.pred_horizon_para + 1)
        inputs = ca.SX.sym('inputs', n_inputs, self.ctrl_horizon_para)
        model_params = ca.SX.sym('model_params', 4)  # Parameter: [veh_x, veh_x_speed, ped_y, ped_y_speed]

        states[:, 0] = model_params[:]

        ped_ = copy.deepcopy(self.ped)
        veh_ = copy.deepcopy(self.veh)

        # set forward kinematic model
        for i in range(self.pred_horizon_para):
            if i in range(self.ctrl_horizon_para):
                states[0, i + 1] = states[0, i] + self.pred_time_step_para * states[1, i] + 0.5 * inputs[0, i] * self.pred_time_step_para ** 2
                states[1, i + 1] = states[1, i] + self.pred_time_step_para * inputs[0, i]

                veh_.x_var = states[0, i]
                veh_.xspeed_ms_var = states[1, i]
                ped_.y_var = states[2, i]
                ped_.yspeed_ms_var = states[3, i]
                states[2, i + 1] = states[2, i] + self.pred_time_step_para * states[3, i]
                states[3, i + 1] = ped_sigmoid_model(ped_, veh_)
            else:
                states[0, i + 1] = states[0, i] + self.pred_time_step_para * states[1, self.ctrl_horizon_para]
                states[1, i + 1] = states[1, i]

                states[2, i + 1] = states[2, i] + self.pred_time_step_para * states[3, i]
                states[3, i + 1] = ped_sigmoid_model(ped_, veh_)

        # set cost function
        obj = 0
        for i in range(self.ctrl_horizon_para):
            obj += self.w1 * inputs[0, i] ** 2
            obj += self.w2 * (self.veh.xspeed_ref_para - states[1, i]) ** 2

            pred_ped_veh_distance = np.sqrt(
                (self.ped.x_var - states[0, i + 1]) ** 2 + (self.veh.y_var - states[2, i + 1]) ** 2)
            obj += self.w3 * 1 / ((pred_ped_veh_distance) ** 4 + 1e-4) * (0.8 ** i)

        # define constraints
        g = []
        for i in range(1, self.pred_horizon_para + 1):
            g.append(states[1, i])

        # define non-linear programming solver
        nlp = {'f': obj, 'x': ca.reshape(inputs, -1, 1), 'p': model_params, 'g': ca.vertcat(*g)}

        opts_setting = {'ipopt.max_iter': 500, 'ipopt.print_level': 0, 'print_time': 0, 'ipopt.acceptable_tol': 1e-8, \
                        'ipopt.acceptable_obj_change_tol': 1e-6}
        nlp_solver = ca.nlpsol('solver', 'ipopt', nlp, opts_setting)

        # set lower bound and upper bound for inputs and constraints
        lbg = []
        ubg = []
        lbx = []
        ubx = []

        for i in range(self.pred_horizon_para):
            lbg.append(0.0)
            ubg.append(self.veh.xspeed_max_para)

            if i in range(self.ctrl_horizon_para):
                lbx.append(self.min_veh_x_acc_para)
                ubx.append(self.max_veh_x_acc_para)

        # set model parameters
        nlp_params = [self.veh.x_var, self.veh.xspeed_ms_var, self.ped.y_var, self.ped.yspeed_ms_var]

        # call solver
        res = nlp_solver(x0=[-2.0] * self.ctrl_horizon_para, p=nlp_params, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

        self.veh_acc_cmd = float(res['x'][0, 0])

    def eval_func(self, params):
        res = 0.0
        k1 = 1
        k2 = 1
        k3 = 5
        k4 = 0

        n = 1 if DEBUG_MODE else len(self.ped_init_x)
        for i in range(n):
            t = 0
            max_veh_acc = 0.0
            min_ped_veh_distance = float('inf')
            ttc = 0

            # reset model state
            self.reset_model_state(i)

            # simulation of motion
            while self.veh.x_var <= self.ped.x_var:
                # debug
                if DEBUG_MODE:
                    print(f'time: {t}')
                    print(f'current ped_x: {self.ped.x_var}')
                    print(f'current ped_y: {self.ped.y_var}')
                    print(f'current ped_yspeed: {self.ped.yspeed_ms_var}')
                    print(f'current veh_x: {self.veh.x_var}')
                    print(f'current veh_xspeed: {self.veh.xspeed_ms_var}')
                    print(f'current veh_acc_cmd: {self.veh_acc_cmd}')
                    print('--------------------')

                # calculate command
                self.model_predictive_control(params)

                self.ped.yspeed_ms_var = social_force_model(self.ped, self.veh)

                # update state
                self.veh.x_var += self.veh.xspeed_ms_var * self.pred_time_step_para + 0.5 * self.veh_acc_cmd * self.pred_time_step_para ** 2
                self.veh.xspeed_ms_var += self.pred_time_step_para * self.veh_acc_cmd
                self.ped.y_var += self.pred_time_step_para * self.ped.yspeed_ms_var

                self.ped_veh_distance = self.ped.x_var - self.veh.x_var
                self.ped_distance_from_road = self.ped.y_var - self.veh.y_var

                # ttc
                if self.ped_distance_from_road >= 0 and self.ped_distance_from_road + self.pred_time_step_para * self.ped.yspeed_ms_var <= 0:
                    ttc = self.ped_veh_distance / (self.veh.xspeed_ms_var + 0.01)
                # t
                t += self.pred_time_step_para
                # min_ped_veh_distance
                if self.ped_distance_from_road >= 0:
                    min_ped_veh_distance = min(min_ped_veh_distance, np.sqrt(self.ped_veh_distance ** 2 + self.ped_distance_from_road ** 2))
                # max_veh_acc
                max_veh_acc = max(max_veh_acc, abs(self.veh_acc_cmd))

            res += k1 * t + k2 * max_veh_acc ** 2 - k3 * min_ped_veh_distance if ttc == 0 else k1 * t + k2 * max_veh_acc - k3 * min_ped_veh_distance + k4 * 1 / ttc
        return res


def opt_func(particles):
    n_particales = particles.shape[0]
    res = []
    for i in range(n_particales):
        obj = ParticleSwarmOptimization()
        res.append(obj.eval_func(particles[i]))
    return np.array(res)

# debug
if DEBUG_MODE:
    params = [0.5, 1.0, 1.0, 1000.0] # parameter of defensive driver
    obj = ParticleSwarmOptimization()
    t1 = time.time()
    result = obj.eval_func(params)
    t2 = time.time()
    print(f'result of debug:{result}')
    print(f'time:{t2-t1}')
else:
    # Set-up hyperparameters
    options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}
    constraints = (np.array([0.1, 0.0, 0.0, 1000.0]),
                   np.array([0.6, 10.0, 10.0, 5000.0])
                   )
    # Call instance of PSO
    optimizer = ps.single.GlobalBestPSO(n_particles=40, dimensions=NUM_PARAMETERS, options=options, bounds=constraints)
    # Perform optimization
    cost, res = optimizer.optimize(opt_func, iters=100)

    print(f'pred_time_step: {res[0]}')
    print(f'w1: {res[1]}')
    print(f'w2: {res[2]}')
    print(f'w3: {res[3]}')


    # save paramters to yaml file
    non_linear_mpc_config_yaml = f"""
    non_linear_mpc:
      ros__parameters:
        pred_time_step: {res[0]}
        w1: {res[1]}
        w2: {res[2]}
        w3: {res[3]}
    """
    non_linear_mpc_config = yaml.safe_load(non_linear_mpc_config_yaml)
    with open('non_linear_mpc_config.yaml', 'w') as file:
        yaml.dump(non_linear_mpc_config, file)
