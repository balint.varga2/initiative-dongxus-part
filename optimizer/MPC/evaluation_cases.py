import numpy as np
from utils.sim_model_objects import Object
from utils.social_force_model import social_force_model
from utils.linear_mpc import LinearMPC
from utils.non_linear_mpc_implicit import NonLinearMPC_implicit
from utils.non_linear_mpc_explicit import NonLinearMPC_explicit
from utils.ppo_control import PPOAgent
from utils.rule_based_controller import RuleBasedController

controller_type = [LinearMPC(), NonLinearMPC_implicit(), NonLinearMPC_explicit(), PPOAgent(), RuleBasedController()]
n_cases = 40


class Evaluation:
    def __init__(self, n_cases):
        self.ped = Object()
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.5
        self.ped.intention_var = 1.0  # between 0 and 1

        self.veh = Object()
        self.veh.xspeed_ref_para = 7.0
        self.veh.xspeed_max_para = 10.0

        self.time_step = 0.1
        self.n_cases = n_cases

        self.count_para = 0

        # self.human_model_mode = [np.random.choice([0, 1]) for _ in range(n_cases)] # 0: constant speed  1: SFM
        self.human_model_mode = [1 for _ in range(n_cases)]  # 0: constant speed  1: SFM

        # set initial state here so that it's consistent for every controller

        # decide if the human will cross road
        self.ped_target_ls, self.intention_ls = [], []
        for _ in range(n_cases):
            is_ped_want_to_cross = np.random.choice([0, 1])
            if not is_ped_want_to_cross:
                self.ped_target_ls.append(1.5)
                self.intention_ls.append(np.random.uniform(0, 0.5))
            else:
                self.ped_target_ls.append(-2.0)
                self.intention_ls.append(np.random.uniform(0.5, 1))

        # human's crossing direction is binary and randomly selected
        self.ped_y_init, self.ped_yspeed_init = [], []
        for _ in range(n_cases):
            random_num = np.random.choice([0, 1])
            self.ped_y_init.append(max(2.0, (3.5 + np.random.normal(0, 0.5))) * (-1) ** random_num)
            self.ped_yspeed_init.append((1.4 + np.random.normal(0, 0.1)) * (-1) ** (1 - random_num))

        self.ped_x_init = [12.5 + np.random.normal(0, 1) for _ in range(n_cases)]
        self.veh_xspeed_init = [6.0 + np.random.normal(0, 0.5) for _ in range(n_cases)]

    def reset(self, case_i):

        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ms_var = self.veh_xspeed_init[case_i]

        self.ped.x_var = self.ped_x_init[case_i]
        self.ped.y_var = self.ped_y_init[case_i]
        self.ped.yspeed_ms_var = self.ped_yspeed_init[case_i]
        self.ped.intention_var = self.intention_ls[case_i]

    def intention_discounting(self):
        is_start_discounting = abs(self.ped.y_var - self.veh.y_var) > 1.5
        if is_start_discounting and abs(self.ped.yspeed_ms_var) < 0.5:
            self.count_para += 1
        else:
            self.count_para = 0
        self.ped.intention_var *= pow(0.95, (self.count_para - 50) * 0.5) if self.count_para >= 50 else 1

    def eval(self, controller):
        score = 0
        n_collision = 0
        for i in range(self.n_cases):
            self.reset(i)
            min_TTC = float('inf')
            time = 0
            max_acc = 0
            k1 = 1.0
            k2 = 1.0
            k3 = 1.0
            while self.veh.x_var < self.ped.x_var:

                # intention discounting
                self.intention_discounting()

                # calculate command for next time step
                veh_acc = controller.eval(self.ped, self.veh)[0]
                if self.ped_target_ls[i] < 0 and not self.human_model_mode[i]:
                    pass
                else:
                    self.ped.yspeed_ms_var = social_force_model(self.ped, self.veh, self.ped_target_ls[i])
                # update state
                self.veh.x_var += self.veh.xspeed_ms_var * self.time_step + 0.5 * veh_acc * self.time_step ** 2
                self.veh.xspeed_ms_var += self.time_step * veh_acc
                self.ped.y_var += self.time_step * self.ped.yspeed_ms_var
                # update results
                if (self.ped.y_var - self.veh.y_var) * self.ped.yspeed_ms_var <= 0:
                    TTC = abs((self.ped.x_var - self.veh.x_var)/(self.veh.xspeed_ms_var + 0.001) -
                               abs(self.ped.y_var - self.veh.y_var)/(abs(self.ped.yspeed_ms_var) + 0.001))
                    min_TTC = min(TTC, min_TTC)

                time += 0.1
                # to avoid Dead-Lock
                if time >= 20.0:
                    break

                max_acc = max(max_acc, abs(veh_acc))

                is_collision = self.ped.x_var - self.veh.x_var < 1.0 and abs(self.veh.y_var - self.ped.y_var) < 1.0 and self.veh.xspeed_ms_var > 1.0
                if is_collision:
                    n_collision += 1
                    score -= 1000
                    break

            score += k1 * min_TTC - k2 * time - k3 * max_acc

        return score / self.n_cases, n_collision


if __name__ == '__main__':
    eval_obj = Evaluation(n_cases)
    for controller in controller_type:
        score, n_collision = eval_obj.eval(controller)
        print('---------------------------------')
        print(f'Current controller: {controller}')
        print(f'Score: {score}, Number of Cases: {eval_obj.n_cases}, Number of accident: {n_collision}')




