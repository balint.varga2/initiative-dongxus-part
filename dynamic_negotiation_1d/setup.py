from setuptools import setup
from glob import glob
import os

package_name = 'dynamic_negotiation_1d'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Install the launch files:
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='yang',
    maintainer_email='ubtef@student.kit.edu',
    description='Publish the result of dynamic negotiation',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'dynamic_negotiation_1d = dynamic_negotiation_1d.dynamic_negotiation_1d:main',
        ],
    },
)
